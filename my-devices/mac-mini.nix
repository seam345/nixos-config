{
  imports =
    [ # Include the results of the hardware scan.
      ../config/machine-specific/mac-mini/hardware-configuration.nix
      ../config/machine-specific/mac-mini/manual-hardware.nix
      ../config/machine-specific/mac-mini/apple-silicon-support
      ../config/roles/development/development.nix
      ../config/roles/personal.nix
      ../config/roles/wifi.nix
    ];
 }