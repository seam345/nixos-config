{
  imports = [
    ../config/machine-specific/pc/hardware-config.nix
    ../config/machine-specific/pc/manual-hardware-config.nix
    ../config/machine-specific/pc/immish.nix
    ../config/roles/development/development.nix
    ../config/roles/personal.nix
    ../config/roles/wifi.nix
    ../config/roles/poppy.nix
    ../config/roles/nas-mount.nix
    ../config/roles/server-atuin.nix
    ../config/roles/server-paperless.nix
  ];
}
