{
  imports = [
    ../config/machine-specific/nas/hardware-config.nix
    ../config/machine-specific/nas/grafana.nix
    ../config/machine-specific/nas/manual-hardware-config.nix
  ];
}
