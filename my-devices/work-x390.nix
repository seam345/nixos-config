{
  imports = [
    # Include the results of the hardware scan.
    ../config/machine-specific/work-x390/manual-hardware-config.nix
    ../config/machine-specific/work-x390/hardware-configuration.nix
    ../config/roles/laptop.nix
    ../config/roles/development/development.nix
    ../config/roles/work.nix
    #    ../config/roles/wifi.nix # todo work this out!
    ../config/roles/work-wifi.nix
  ];
}
