{
  imports =
    [ # Include the results of the hardware scan.
      ../config/machine-specific/little-my/manual.nix
      ../config/roles/development/development.nix
      ../config/roles/personal.nix
      ../config/roles/wifi.nix
      ../config/roles/laptop.nix
      ../config/roles/nas-mount.nix
    ];
 }