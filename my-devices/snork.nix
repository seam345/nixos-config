{
  imports = [ # Include the results of the hardware scan.
    ../config/machine-specific/snork/manual-hardware-config.nix
    ../config/machine-specific/snork/grafana.nix
    ../config/machine-specific/snork/watchman.nix
#    ../config/machine-specific/snork/washing-log.nix
#    ../config/machine-specific/snork/node-red.nix
    ../config/roles/zigbee2mqtt.nix
    ../config/machine-specific/snork/vaultwarden.nix
  ];
}
