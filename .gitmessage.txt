


# # Git convention
# following a similar style to conventional commits but with different types as a lot of them make no sense
# - `Update:` A commit that changes the base nixpkgs is considered and update
# - `Update(<pkg-name>):` An update to a package or list of packages `Update(<pkg-name>,<pkg-name2>):`
#   - `Update(wifi):` An update to the WI-FI config
# - `New Device:` A commit that is primarily adding a new device to the config
# - `Package new(<efected devices>):` Adding any new package
# - `Package scope(<pkg-name>):` Scope of package as changed, e.g. gets applied to more or less devices
# - todo maybe remove the service ones i havent used them and activly jst use the package new
# - `Package config(<pkg-name>):` Updating or changing any config for a package
# - `Service new:` Adding any new service
# - `Service config(<service-name>):` Updating or changing any config for a service
# - `Service scope(<service-name>):` Scope of service as changed, e.g. gets applied to more or less devices
# - `Security(<name>):` changed SSH key `Secutiry(ssh)` or something
# - `Inconsistency fix:` A commit that addresses some inconsistency that shouldn't have happened
# - `Refactor:` any moving around of code
# - `Typos:` fixing up any typos in comments
# - `Doc:` updating any comments/readme anything that helps describe what's happening
#