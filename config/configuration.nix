{ pkgs, ... }:
let
  yubikeyPubSSHKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDkrrhqdyMysvZ1VTsRpuVbpZ4FpEGBzJLdDhj5GMNbCJvNC8jgLnERI1Ubb1E18B9e9+ArzmXlEvSHMI6g/vdADa2UPQnDKEtm+ZaLhLCqZaiHPshbAs/q70lqggaswHYc/RW5uT9ELqSkSO2kOfotDK7gx9TYukmqIoZXY60r/RQBvWDz0G6mswIQ6hdJv4H9boPbKT49E14T7ycoJPkGKRGKCnDTmA19AgzF41+iLQFY+PfBpICpBix2NEkKRB2ZR0rhHkq9EgCJq5ZHAxdMJ57PXcxn12m0JB6gjx6MskQ3rlQbr245f/+OwGgbdBmOdlbsB6stQlOCKL1kR/wOMVni+236SmHGACEQXGfVhYdlE3XU+18Ju+vY1Su09tOtLJP1/Ys/2N2oh2C+cctK36K56wWjB0H+IFIDaaBoL0xvWRPBI8S+xtWwYGsUCP5vJBVNBteGww0SksxrgA/q5nKXLVi5BYBFWhQndhP4WnkkVSOSXqNCZKH89+yJmRs= cardno:000614465514";
  laptopPubSSHKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCswlyW1qQ6QCdkchTualqdLgsjaR177Ud9f0+z++dkOnfRQLljcCljh1fdR1pGahBlqzonF4zMXAF873FG/d/H/TwRQQyWtnb4AriddOdV3jJnoKafvHDUxBb40LtvRM6c8d4klyMgw2+EK223OTsQifh8NIuZkqJqyhFuLX5Gtbz96EmXWmbiQsUBL5f131etKZCfFSyX3HbGTeUSCg4on//QXUQER60CW9jr7xeSrnwTuWls2l19IkWQS08/7/GKqRKriLkpgcHIDBpRYc7MUyaZCzEB/3DJ/l/fG8BbtzMCXaHQPMaAOGRI9eQIeWTiyAQf7Pb3RoGudp780845xCLvlcusv+ehR/v6btMssDSBJVkc2axLZdQN8n2muz+tcvVxWA/BUiWpB16/IHY3bBx7v9zvDilEq1orCQDIrq6WlpgssX5oX61dCIDfRi+3amZ1km2fm89NBV39so24wmr8LUlwf/397w+yAnpHr23qCWgT3fMNIvtAhdDjV2M= sean@lenovo-laptop";
  x390PubSSHKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDF2KloKNkyp6dhRX07+G5AVCvc1Ac6h8Y+d6PsFxly/gzsvaJ9X8dB20fIfxWYWKUWVS6Ju0ipe3VPfS8FJWQsaoh5pPEQFG+vlFffiHrK+aRoMGGulTXCAlM4O1aaVBdrQ6IZ7PnTUJ3GbwKpDaTlweLneDcMNdtyi9WXdBylYj14Hru4/TyAHDgbTGofBdR7p8HzfIaU9TlfTqURUKRqKBddLWHRz2uOGFfSl5FtFd36pWvruNP7oK0itbNYUYZowBNrAnXGHe3Gv/olxZS2Ism9E20VUgNfSdHQDDR1g8Q17zLf0R9njMVO2i2DhBCHtZqqIIdAlMjkV/MZ9If1OPY0vXm3R0iank8uw7kl64Gmm71xBzRUAtiY47To3SNBxBgjiwaeW+FeG3/jywHnE4lZNBIEe4pNFLCGXUGTyOW8xM/8SWXdaRc842wxYgFrPlpO6CM/ZF1amY1Q1Yt0FwFKFDN5uw76nYgfl6RzVJzV5Vkh4DGa4quctLTHwp0= sean@work-x390";
  gitlabSSHKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCyxsiI7j/tHY9lerD7QIkn9r0W80WtUHHb69NQpKEvhH2zTjNLANflmQtvMROoN6SpHnFndpW9B4jLTUUa34BIWpoRPnJ5iht7ljrHW8vyIfu/l9+uz1BJeX5yvjr0hOzlxOFGs6L/Zfm/m01PzGHD2DtvvV0Kgr7cXHrgP374ALb8mZcm7y1QHb0CeUuIwUruc2i4MlWTEqQWW4hMRMCCu1WNGXYZ2/IrcWMnZ96sYiiF6pp3BY/EbQVLRlWvnGZFk307gu91pC/V4nK9dBoSfYo8yCEAHmwsXK7yLA5g6b4NIrncraV0s9SYdFelTbNhr4yV/7I13NQnCX8BouhsL9lxCadPmcOukiW05LvcXF/f2VnrAFc+oLMdnvUzKZDlc364ssUBWyIOku4/d/xGimc8yGFOcwZhwp7kc4RSF03DoiJ0kcWSRMyoaVdytSI2D/mU/nao+b3z2RZSZ7RMBhn0+tEAyXyFkVx6Yqi+Y2cqBkxhGyo8YwKAaLdCoC0= sean@pc";
  pcSSHKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCzidwAurlfutFndPDRNtuk0ZwmnT0meaBDUONzpIdjmcXNw8uWHrXTG0VRYOQFJYgpVsr9Fj2ZX3aDeWKyDZI0EqMV4FYw8locMRMkhM4+Vb+NL5M1F5VCCqVy/eFoIfyfpFIpUetaL9bUK0uaKlx/a7fzl9gqcny4Pbpm++fNGunimJyafjwvIox+3l8pZHNlnN7bDZZJL+9wmppiYGOQ4DL817W2mGBhlu5Hx+RjZL4G8PfRGyGd4fanJMz8pIiYYwb2nU71D+7zDubma/xIsLXZ/EzPuhmMm3xQEcwl1jlySs3Vft1DCXdfM44UnWh+FI4nywv6Tiy7GHsoTU6KGUtbiGj9hlTxpUQNQzShcPnIcM8BQtLuQ+hkXOAbUVv3ck31+VG+r4XLAPoGMwFC3JjHOf/655qGKUD4nGl3AAVpSTG20VbKqD9RP55GNv58n+lpO0xWful0CS0nVTGwjrdP7gFqH7n2eocKUBh8gW21qUjPjMirXWWkyngIGOk= sean@pc";
in
{
  imports = [
    ../imports.nix # this is pulling from the machine after krops has already copied over the file
  ];

  # serve nix store to allow sharing across devices (not sure how this works yet)
  nix.sshServe.enable = true;
  nix.sshServe.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFJzM/UHBs8lYU3QhntGHV2+sEagihqrzWfXoxhMTNHH root@pc"
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDNvKXWzqb3hRmzcMATVsDZT7TMRTbgUNlWLoe9YO5YH+DAsEY751oiyBc2xyoCoGxRSULv2P51i/wEo+2YL41sdy7iUYyadBgTaWhrlI6y6FGG4A6X78JFwWeWDcRAm27i+tlu8iVcopQZpWnU8zVi1seipZaFpDI3o77FfoVc8WIGNyV2Jx9mT6DnPRgCPvoR4OxZQg33KXuZ341nHIibiJPNkmMZXyUEeNVhG5aBIwCkRMdcJgwPhXxp0fcU3+o9O4HVgYppYinIxCgr6WVaZ2xgO20DpPqZCdWBK7omcO9qZSwxw+Bz5WpewTyFYdt93kGUh7LAA5rjZVXMkGiO1tKLD13UcRMJO/CLmd/GyJGckQ4PPf37uzAx8wBX6IUmJEHGiOfKFmaz68HtyAtStZ3RO9eKzoBe6584QPAbr9DHejZeX8E1auZQfq9ioz2xwMGh0Hzd8YpKJD68WwKdTFglvD5b5DAez+wZiFs1Qd8XP1LEGvgKqKsRF3Oixg0= root@lenovo-laptop"
  ];
#  nix.sshServe.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBYBu9LSNovV+7o6IWqJgEu4XR2xijFvwzmzLNKSzkVp krops@100.81.147.45" ];
  nix.settings.substituters = [
#    "ssh://nix-ssh@100.108.38.15?priority=1"
#    "ssh://nix-ssh@100.81.147.45?priority=0"
#    "http://100.81.147.45:8092?priority=0&trusted=true"

#    "ssh://nix-ssh@100.117.29.94?priority=2" # laptop might need to remove if i cant get around failed connection stopping execution
  ];
  nix.settings.trusted-substituters = [
#    "100.108.38.15" # nas
    "ssh://nix-ssh@100.81.147.45?priority=0"
    "100.81.147.45" # pc
    "100.117.29.94" # laptop (online lesss but could have some bleeding edge builds so occasionally useful
  ];



  security.sudo.enable = true;
  security.sudo.extraConfig = ''
    krops ALL=(ALL) /run/current-system/sw/bin/nixos-rebuild -I /var/src switch
    krops ALL=(ALL) /run/current-system/sw/bin/nixos-rebuild -I /var/src switch --no-build-nix
  '';

  users.users.krops = {
    # remember krops needs to have access to /var/src
    isNormalUser = true;
    home = "/home/krops";
    description = "krops";
    # does order mater, no why does work laptp prefer yubikey :(
    openssh.authorizedKeys.keys = [
      "${laptopPubSSHKey}"
      "${yubikeyPubSSHKey}"
      "${gitlabSSHKey}"
      "${pcSSHKey}"
      "${import ./vars/ssh-public-keys/little-my.nix}"
    ];
  };

  time.timeZone = "Europe/London";

  services.tailscale.enable = true;
  networking.firewall.checkReversePath = "loose";
  services.openssh.enable = true;
    services.openssh.settings.PasswordAuthentication = false;
#  services.openssh.passwordAuthentication = false;
  nixpkgs.overlays = [ (self: super: { local = import ./my-pkgs { pkgs = super; }; }) ];
  environment.systemPackages = [ pkgs.git pkgs.git-lfs pkgs.vim pkgs.tailscale pkgs.tree pkgs.local.required-scripts pkgs.fish pkgs.zellij ];

  programs.fish.enable = true;
  users.users.root = {
    openssh.authorizedKeys.keys = [
      "${yubikeyPubSSHKey}"
    ];
    shell = pkgs.fish;
  };
  system.stateVersion = "22.11";
}


# new pc install
# use normal install media get bootable version with internet access
# reboot
# clone config
# append this config to  /etc/nixos/configuration
# fixup syntax issues
# `nixos-rebuild switch`
# move to already setup device
# ssh into root with yubikey
# copy hardware setup, confirm any differences in config that may or may not be useful
# SOPS!
# on system

# ssh root@<host>
# nix-channel --update  # hmmm do i want todo this is will be a different commit than krops?
# nix-shell  -p ssh-to-pgp --run "ssh-to-pgp -i /etc/ssh/ssh_host_rsa_key -o server01.asc"
# copy output to add to the keys list
# cd config/
# rsync root@<host>:/root/server01.asc public-keys/<host-name>
# gpg --import public-keys/*
# update any yaml files
# nix-shell -p sops --command 'sops  updatekeys <yaml_file>'

# login to root (yubikey needed) or krops
# [root@nixos:~]# passwd krops
# [krops@nixos:~]# passwd
# update krops password
# add /var/src and make it krops writeable
# [root@nixos:~]# mkdir /var/src
# [root@nixos:~]# chown krops /var/src/
# update krops
# run krops

# subsitutors i will need to generate an ssh key and add it to the list of keys aloud (in krops user)
