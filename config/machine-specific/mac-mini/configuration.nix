# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:
let
  laptopPubSSHKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCswlyW1qQ6QCdkchTualqdLgsjaR177Ud9f0+z++dkOnfRQLljcCljh1fdR1pGahBlqzonF4zMXAF873FG/d/H/TwRQQyWtnb4AriddOdV3jJnoKafvHDUxBb40LtvRM6c8d4klyMgw2+EK223OTsQifh8NIuZkqJqyhFuLX5Gtbz96EmXWmbiQsUBL5f131etKZCfFSyX3HbGTeUSCg4on//QXUQER60CW9jr7xeSrnwTuWls2l19IkWQS08/7/GKqRKriLkpgcHIDBpRYc7MUyaZCzEB/3DJ/l/fG8BbtzMCXaHQPMaAOGRI9eQIeWTiyAQf7Pb3RoGudp780845xCLvlcusv+ehR/v6btMssDSBJVkc2axLZdQN8n2muz+tcvVxWA/BUiWpB16/IHY3bBx7v9zvDilEq1orCQDIrq6WlpgssX5oX61dCIDfRi+3amZ1km2fm89NBV39so24wmr8LUlwf/397w+yAnpHr23qCWgT3fMNIvtAhdDjV2M= sean@lenovo-laptop";
in
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./apple-silicon-support
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = false;

  # networking.hostName = "nixos"; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  # networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  networking.wireless.iwd = {
    enable = true;
    settings.General.EnableNetworkConfiguration = true;
  };

  services.tailscale.enable = true;
  services.openssh.enable= true;
  services.openssh.settings.PasswordAuthentication = false;
  # Set your time zone.
  # time.timeZone = "Europe/Amsterdam";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkb.options in tty.
  # };

  # Enable the X11 windowing system.
  # services.xserver.enable = true;


  

  # Configure keymap in X11
  # services.xserver.xkb.layout = "us";
  # services.xserver.xkb.options = "eurosign:e,caps:escape";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # hardware.pulseaudio.enable = true;
  # OR
  # services.pipewire = {
  #   enable = true;
  #   pulse.enable = true;
  # };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.users.alice = {
  #   isNormalUser = true;
  #   extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  #   packages = with pkgs; [
  #     firefox
  #     tree
  #   ];
  # };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  # environment.systemPackages = with pkgs; [
  #   vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
  #   wget
  # ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system - see https://nixos.org/manual/nixos/stable/#sec-upgrading for how
  # to actually do that.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "24.11"; # Did you read the comment?




  programs.fish.enable = true;
  users.users.root = {
    openssh.authorizedKeys.keys = [
      "${laptopPubSSHKey}"
    ];
    shell = pkgs.fish;
  };
}


# new pc install
# use normal insall media get bootable version with internet accces
# reboot
# clone config
# append this config to  /etc/nixos/configuration
# fixup syntax issues
# `nixos-rebuild switch`
# move to already setup device
# ssh into root with yubikey
# copy hardware setup, confirm any diferences in config that may or may not be useful
# SOPS!
# on system

# ssh root@<host>
# nix-channel --update  # hmmm do i want todo this is will be a different commit than krops?
# nix-shell  -p ssh-to-pgp --run "ssh-to-pgp -i /etc/ssh/ssh_host_rsa_key -o server01.asc"
# copy output to add to the keys list
# cd config/
# rsync root@<host>:/root/server01.asc public-keys/<host-name>
# gpg --import public-keys/*
# update any yaml files
# nix-shell -p sops --command 'sops  updatekeys <yaml_file>'

# login to root (yubikey needed) or krops
# [root@nixos:~]# passwd krops
# [krops@nixos:~]# passwd
# update krops password
# add /var/src and make it krops writeable
# [root@nixos:~]# mkdir /var/src
# [root@nixos:~]# chown krops /var/src/
# update krops
# run krops

# subsitutors i will need to generate an ssh key and add it to the list of keys aloud (in krops user)
