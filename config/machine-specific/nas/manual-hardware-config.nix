{ pkgs, lib, config, ... }:
{
  networking.hostName = "nas";
  imports = [
    ../../roles/home-manager.nix
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/disk/by-id/ata-Samsung_SSD_860_EVO_250GB_S4CJNJ0N316990E"; # or "nodev" for efi only

  boot.kernel.sysctl."net.ipv6.conf.all.forwarding" = 1;
  boot.kernel.sysctl."net.ipv4.ip_forward" = 1;

  environment.systemPackages = [ pkgs.zfs pkgs.rclone ];
  boot.supportedFilesystems = [ "zfs" ];
  networking.hostId = "9fff5310";
  services.zfs.autoScrub.enable = true;
  services.zfs.autoScrub.interval = "monthly";
  services.zfs.autoSnapshot.enable = true;

  #  helpful zfs commands
  # zpool import # lists pools not currently available on device
  # pool import -f <pool name> # mount pool
  # zfs set mountpoint=legacy <pool name> # set to legacy mode for nixos
  # zfs set com.sun:auto-snapshot=true my-data-backup-pool
  # zfs list -t snapshot # list snapshots
  # zfs list -ro space <pool-name-optional> # show space on disk aswell as snapshot space
  networking.firewall.allowedTCPPorts = [
    8200 # idk
    80 # nginx
    443 # nginx
    8080 # scrutiny
    8086 # influxDB
    4100 # castnow
    4101 # castnow
    4102 # castnow
    4103 # castnow
    8384 # syncthing
    8385 # syncthing
    22000 # syncthing
  ];

  # Syncthing ports
  networking.firewall.allowedUDPPorts = [
    22000 # syncthing
    21027 # syncthing
  ];

  services.nginx = {
    enable = true;
    virtualHosts."_" = {
      default = true;
      extraConfig = "autoindex on;";
      root = "/data/media/";
      listen = [
        {
          addr = "100.108.38.15";
          port = 80;
        }
      ];
    };
    virtualHosts."nas.local" = {
      default = true;
      extraConfig = "autoindex on;";
      root = "/data/media/";
      listen = [
        {
          addr = "192.168.1.69";
          port = 80;
        }
      ];
    };
    virtualHosts."narrow.boat" = {
      default = true;
      root = "/data/backed-up/markdown-notes/narrow-boat-website";
      listen = [
        {
          addr = "100.108.38.15";
          port = 8123;
        }
      ];
    };
  };

  users.users.data = {
    isNormalUser = true;
    description = "able to access everything under /data";
    openssh.authorizedKeys.keys = [
      "${import ../../vars/ssh-public-keys/little-my.nix}"
      "${import ../../vars/ssh-public-keys/pc.nix}"
      # "${import ../../vars/ssh-public-keys/lenovo-laptop.nix}"
    ];
  };

  virtualisation.docker.enable = true;
  systemd.services."scrutiny" = {
    description = "scrutiny";
    wantedBy = [ "multi-user.target" ];
    after = [ "docker.service" "docker.socket" ];
    requires = [ "docker.service" "docker.socket" ];
    serviceConfig = {
      ExecStart = "${pkgs.docker}/bin/docker run --rm -p 8080:8080 -p 8087:8086   -v /etc/scrutiny:/opt/scrutiny/config   -v /etc/influxdb2:/opt/scrutiny/influxdb   -v /run/udev:/run/udev:ro   --cap-add SYS_RAWIO   --device=/dev/sda --device=/dev/sdb --device=/dev/sdc --device=/dev/sdd --device=/dev/sde --device=/dev/sdf   --name scrutiny   ghcr.io/analogj/scrutiny:master-omnibus";
    };
    preStop = "${pkgs.docker}/bin/docker stop scrutiny";
    # running with --rm so reload is a stop and remake
    reload = "${pkgs.docker}/bin/docker stop scrutiny && ${pkgs.docker}/bin/docker run --rm -p 8080:8080 -p 8086:8086   -v /etc/scrutiny:/opt/scrutiny/config   -v /etc/influxdb2:/opt/scrutiny/influxdb   -v /run/udev:/run/udev:ro   --cap-add SYS_RAWIO   --device=/dev/sda --device=/dev/sdb --device=/dev/sdc --device=/dev/sdd --device=/dev/sde --device=/dev/sdf   --name scrutiny   ghcr.io/analogj/scrutiny:master-omnibus";
  };

  services.influxdb2.enable = true;
  systemd.services.influxdb2.after = lib.mkForce [ "tailscaled.service" ];
  systemd.services.influxdb2.serviceConfig = { RestartSec = 60; };
  systemd.services.influxdb2.startLimitIntervalSec = 0;

  # can't remember what this is for
  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  };
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
      vaapiVdpau
      libvdpau-va-gl
    ];
  };

  # Syncthing
  services.syncthing.enable = true;
  #     networking.firewall.allowedTCPPorts = [ 8384 22000 ];
  #     networking.firewall.allowedUDPPorts = [ 22000 21027 ];
  services.syncthing.guiAddress = "0.0.0.0:8384";
  services.syncthing.user = "data";
  services.syncthing.dataDir = "/data";

  boot.kernel.sysctl."fs.inotify.max_user_instances" = 2147483647;
}
