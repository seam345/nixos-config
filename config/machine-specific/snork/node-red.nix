{
  services.node-red = {
    enable = true;
    openFirewall = true;
    withNpmAndGcc = true;
  };


  users.users.datasync = {
    extraGroups = [ "node-red" ];
  };
}
