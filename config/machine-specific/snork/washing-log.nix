{ config, pkgs, lib, ... }:
{

  imports = [
    ../../roles/sops.nix
  ];
#  sops
  sops.secrets."snork yaml file" = {
    sopsFile = ./watchman-secrets.yaml;
#    key = "";
  };

  systemd.services."mqtt-wash-log" = {
    enable = true;
    description = "";
    environment = {
      WATCHMAN_FILE = config.sops.secrets."snork yaml file".path;
    };
    after = [ "tailscaled.service" ];
    wantedBy = [ "multi-user.target" ]; # needed to enable during boot
    serviceConfig = {
      ExecStart = "${pkgs.local.boat-coordinator.washing-log}/bin/wash_counter";
      Restart = "always";
      RestartSec = 60;
      StartLimitIntervalSec = "300"; # it has a 10 min retry internally, if something is failing before that there is a bigger problem so setting this to 5 min
      # StartLimitAction =  # todo work out a nice thing to trigger if start limit it hit, maybe a push notification
    };
  };
}
