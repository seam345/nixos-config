{
  imports = [
    ../boards/rpi4.nix
  ];
  networking = {
    hostName = "snork";
  };
}
