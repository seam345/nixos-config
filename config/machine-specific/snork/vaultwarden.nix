{ config, pkgs, lib, ... }:
{
  imports = [
    ../../roles/syncthing-user.nix
    ../../roles/sops.nix
  ];
  networking.firewall.allowedTCPPorts = [
    443
    8812
    3012
    8384 ## syncthing for backup
  ];

  sops.secrets."variables" = {
    sopsFile = ./vaultwarden-secrets.yaml;
  };

  services.vaultwarden = {
    enable = true;
    backupDir = "/var/lib/bitwarden_rs/backup";
    environmentFile=config.sops.secrets."variables".path;
    config = {
      ROCKET_ADDRESS="100.66.73.33";
      WEB_VAULT_FOLDER = "${pkgs.bitwarden_rs-vault}/share/vaultwarden/vault";
      WEB_VAULT_ENABLED = true;
      WEBSOCKET_ENABLED = true;
      WEBSOCKET_ADDRESS = "100.66.73.33";
      WEBSOCKET_PORT = 3012;
      SIGNUPS_VERIFY = true;
      SMTP_PORT = 587;
      SMTP_SECURITY= "starttls";
      SMTP_TIMEOUT = 15;
      ROCKET_PORT = 8812;
    };
  };

  environment.systemPackages = [
    pkgs.bitwarden_rs-vault
  ];

  users.users."vaultwarden" = {
    createHome = true;
#    linger = true; #  this should exist but nix complans... ill set it manually
    home = "/var/lib/bitwarden_rs";
  };

  home-manager.users."vaultwarden" = {
    home = { stateVersion = "22.11"; };
    services.syncthing.enable = true;
    services.syncthing.extraOptions = [
      "--gui-address=0.0.0.0:8384"
    ];
  };
}
