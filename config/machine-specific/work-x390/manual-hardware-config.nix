{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.initrd.kernelModules = [ "vfat" "nls_cp437" "nls_iso8859-1" "usbhid" ];
  boot.initrd.luks.yubikeySupport = true;
  boot.initrd.luks.devices = {
    "VolGroup" = {
      device = "/dev/nvme0n1p6";
      preLVM = true;
      yubikey = {
        slot = 1;
        twoFactor = true;
        storage = {
          device = "/dev/nvme0n1p5";
        };
      };
    };
  };

  networking.hostName = "work-x390"; # Define your hostname.
  virtualisation.libvirtd.enable = true;
}
