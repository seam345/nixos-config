{ pkgs, ... }:
{
  networking.hostName = "little-my"; # Define your hostname.
  imports = [
      ./hardware-configuration.nix
  ];
  boot.loader.grub = {
    enable = true;
    zfsSupport = true;
    efiSupport = true;
    efiInstallAsRemovable = true;
    mirroredBoots = [
      { devices = [ "nodev"]; path = "/boot"; }
    ];
  };
  networking.hostId="01fa5932";

}
