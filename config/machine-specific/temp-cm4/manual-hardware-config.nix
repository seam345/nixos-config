{ pkgs, ... }: {
  imports = [
    ../boards/rpi4.nix
    "${builtins.fetchGit { url = "https://github.com/NixOS/nixos-hardware.git"; }}/raspberry-pi/4"
  ];

  #  imports = [
  #      .../nixos-hardware/raspberry-pi/4
  #    ];

  hardware.raspberry-pi."4".fkms-3d.enable = true;
  networking = {
    hostName = "temp-cm4"; # Define your hostname.
  };

  services.xserver.enable = true;
  services.xserver.desktopManager.kodi.enable = true;
  services.xserver.displayManager.autoLogin.enable = true;
  services.xserver.displayManager.autoLogin.user = "kodi";
  users.extraUsers.kodi.isNormalUser = true;

  # the bellow may be faster on PI
  #    users.extraUsers.kodi.isNormalUser = true;
  #      services.cage.user = "kodi";
  #      services.cage.program = "${pkgs.kodi-wayland}/bin/kodi-standalone";
  #      services.cage.enable = true;

  networking.firewall = {
    allowedTCPPorts = [ 8080 ];
    allowedUDPPorts = [ 8080 ];
  };


  powerManagement.enable = false;
  #services.xserver = {
  #    enable = true;
  #    displayManager.lightdm.enable = true;
  #    desktopManager.gnome3.enable = true;
  #    videoDrivers = [ "fbdev" ];
  #  };


  #     an overlay to enable raspberrypi support in libcec, and thus cec-client
  #    nixpkgs.overlays = [
  #      (self: super: { libcec = super.libcec.override { inherit (self) libraspberrypi; }; })
  #    ];

  # install libcec, which includes cec-client (requires root or "video" group, see udev rule below)
  # scan for devices: `echo 'scan' | cec-client -s -d 1`
  # set pi as active source: `echo 'as' | cec-client -s -d 1`
  #    environment.systemPackages = with pkgs; [
  #      libcec
  #    ];
  #
  #    services.udev.extraRules = ''
  #        allow access to raspi cec device for video group (and optionally register it as a systemd device, used below)
  #      SUBSYSTEM=="vchiq", GROUP="video", MODE="0660", TAG+="systemd", ENV{SYSTEMD_ALIAS}="/dev/vchiq"
  #    '';

  # optional: attach a persisted cec-client to `/run/cec.fifo`, to avoid the CEC ~1s startup delay per command
  # scan for devices: `echo 'scan' &gt; /run/cec.fifo ; journalctl -u cec-client.service`
  # set pi as active source: `echo 'as' &gt; /run/cec.fifo`
  #    systemd.sockets."cec-client" = {
  #      after = [ "dev-vchiq.device" ];
  #      bindsTo = [ "dev-vchiq.device" ];
  #      wantedBy = [ "sockets.target" ];
  #      socketConfig = {
  #        ListenFIFO = "/run/cec.fifo";
  #        SocketGroup = "video";
  #        SocketMode = "0660";
  #      };
  #    };
  #    systemd.services."cec-client" = {
  #      after = [ "dev-vchiq.device" ];
  #      bindsTo = [ "dev-vchiq.device" ];
  #      wantedBy = [ "multi-user.target" ];
  #      serviceConfig = {
  #        ExecStart = ''${pkgs.libcec}/bin/cec-client -d 1'';
  #        ExecStop = ''/bin/sh -c "echo q &gt; /run/cec.fifo"'';
  #        StandardInput = "socket";
  #        StandardOutput = "journal";
  #        Restart="no";
  #    };
}
