{ config, pkgs, lib, ... }:
let
  telegrafPort = 9003;
in
{
  networking.firewall.allowedTCPPorts = [ 9090 8088 8086 ];
  services.prometheus = {
    retentionTime = "2y"; # Idealy I'd like to set retaintion by space but i dont see it in the nix config so I'll wait a while todo come back to
    enable = true;
    listenAddress = "100.89.209.13";
    webExternalUrl = "http://100.89.209.13:9090";
    extraFlags = [ ''--web.cors.origin=".*"'' ];
    exporters = {
      node = {
        enable = true;
        enabledCollectors = [ "systemd" ];
        port = 9002;
      };
    };
    scrapeConfigs = [
      {
        job_name = "chrysalis";
        static_configs = [{
          targets = [ "127.0.0.1:${toString config.services.prometheus.exporters.node.port}" ];
        }];
      }
      {
        job_name = "nas";
        static_configs = [{
          targets = [ "100.108.38.15:${toString config.services.prometheus.exporters.node.port}" ];
        }];
      }
      {
        job_name = "telegraf";
        static_configs = [{
          targets = [ "127.0.0.1:${toString telegrafPort}" ];
        }];
      }
    ];
  };
  systemd.services.prometheus.after = lib.mkForce [ "tailscaled.service" ];
  systemd.services.prometheus.serviceConfig = { RestartSec = 60; };
  systemd.services.prometheus.startLimitIntervalSec = 0;

  services.telegraf = {
    enable = true;

    extraConfig = {
      inputs.mqtt_consumer = {
        servers = [ "tcp://100.96.69.67:1883" ];
        topics = [ "zigbee2mqtt/#" "esp32/#" "shelly/+/tele/+" ];
        data_format = "json";
        json_string_fields = [ "water_leak" "contact" "occupancy" ];
        qos = 2;
      };

      outputs = {
        prometheus_client = {
          listen = "127.0.0.1:${toString telegrafPort}";
          metric_version = 2;
          export_timestamp = true;
          expiration_interval = "5m";
        };
      };
    };
  };

}
