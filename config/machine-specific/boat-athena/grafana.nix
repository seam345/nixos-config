{
  imports = [
    ../../roles/grafana.nix
  ];
  services.grafana.settings.server.http_addr = "${import ../../vars/ips/boat-athena.nix}";
}
