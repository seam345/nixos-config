{ config, pkgs, lib, ... }:
let
  telegrafPort = 9003;
in
{
  imports = [
    ../../roles/sops.nix
  ];
  sops.secrets.influx-power-token = {
    sopsFile = ./influx-secrets.yaml;
  };
  sops.secrets.influx-sensors-token = {
    sopsFile = ./influx-secrets.yaml;
  };

  networking.firewall.allowedTCPPorts = [ 9090 8088 8086 ];
  services.influxdb2.enable = true;

  systemd.services.influxdb2.after = lib.mkForce [ "tailscaled.service" ];
  systemd.services.influxdb2.serviceConfig = { RestartSec = 60; };
  systemd.services.influxdb2.startLimitIntervalSec = 0;





  nixpkgs.overlays = [ (self: super: { local = import ../../my-pkgs { pkgs = super; }; }) ];
  systemd.services."victron-influx-importer" = {
    enable = true;
    description = "";
    environment = {
      TOKEN_FILE = config.sops.secrets.influx-power-token.path;
    };
    after = [ "tailscaled.service" ];
    wantedBy = [ "multi-user.target" ]; # needed to enable during boot
    serviceConfig = {
      ExecStart = "${pkgs.local.victron-influx-importer}/bin/test-influx";
      Restart = "always";
      RestartSec = 60;
      StartLimitIntervalSec = "300"; # it has a 10 min retry internally, if something is failing before that there is a bigger problem so setting this to 5 min
      # StartLimitAction =  # todo work out a nice thing to trigger if start limit it hit, maybe a push notification
    };
  };

  systemd.services."esp32-influx-importer" = {
    enable = true;
    description = "";
    environment = {
      TOKEN_FILE = config.sops.secrets.influx-sensors-token.path;
    };
    after = [ "tailscaled.service" ];
    wantedBy = [ "multi-user.target" ]; # needed to enable during boot
    serviceConfig = {
      ExecStart = "${pkgs.local.esp32-influx-importer}/bin/test-influx";
      Restart = "always";
      RestartSec = 60;
      StartLimitIntervalSec = "300"; # it has a 10 min retry internally, if something is failing before that there is a bigger problem so setting this to 5 min
      # StartLimitAction =  # todo work out a nice thing to trigger if start limit it hit, maybe a push notification
    };
  };

}
