{
  imports = [
    ../boards/rpi4.nix
    ../../roles/home-manager.nix
  ];
  networking = {
    hostName = "boat-athena"; # Define your hostname.
  };

  # Syncthing
  services.syncthing.enable = true;
  services.syncthing.systemService = false;
  networking.firewall.allowedTCPPorts = [
    8384 # grafana gui
    8385 # influxdb2 gui
    8386 # prometheus gui
    22000 # syncthing
  ];
  networking.firewall.allowedUDPPorts = [ 22000 21027 ];

  users.users."grafana".createHome = true;
#  users.users."grafana".home = "/var/lib/grafana";
  home-manager.users.grafana = {
    home = { stateVersion = "22.11"; };
    services.syncthing.enable = true;
    services.syncthing.extraOptions = [
      "--gui-address=0.0.0.0:8384"
    ];
  };

  users.users."influxdb2".createHome = true;
  users.users."influxdb2".home = "/var/lib/influxdb2";
  home-manager.users.influxdb2 = {
    home = { stateVersion = "22.11"; };
    services.syncthing.enable = true;
    services.syncthing.extraOptions = [
      "--gui-address=0.0.0.0:8385"
    ];
  };

  users.users."prometheus".createHome = true;
  users.users."prometheus".home = "/var/lib/prometheus";
  home-manager.users.prometheus = {
    home = { stateVersion = "22.11"; };
    services.syncthing.enable = true;
    services.syncthing.extraOptions = [
      "--gui-address=0.0.0.0:8386"
    ];
  };

}
