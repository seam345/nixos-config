{ pkgs, lib, config, ... }:
let
  composefile = "immish-docker-compose.yaml";
  photos-gid = 988;
  photos-gid-string = "988";
  photos-uid = 990;
  photos-uid-string = "990";
in
{
  sops.secrets."variables" = {
    sopsFile = ./immish-secrets.yaml;
  };

  users.groups.photos = {
    gid = photos-gid;
  };
  users.users.photos =  {
    isSystemUser = true;
    linger = true;
    createHome = true;
    home = "/home/photos";
    description = "Photo storage user";
    group = "photos";
    uid = photos-uid;
  };

  # Syncthing as user systemd service
  services.syncthing.enable = true;
  services.syncthing.systemService = false;
  networking.firewall.allowedTCPPorts = [
    8386 # synthing
    22000 # syncthing
    2283 # immich
  ];
  networking.firewall.allowedUDPPorts = [ 22000 21027 ];
  home-manager.users.photos = {
    home = { stateVersion = "22.11"; };
    services.syncthing.enable = true;
    services.syncthing.extraOptions = [
      "--gui-address=0.0.0.0:8386"
    ];
  };


  # upgrading breaking changes... so make the changes, then i guess nix rebuild this doesnt restart the service (nicely)
  #
  # consider snapshot `sudo zfs snapshot data/photos@beforeupdate3`
  virtualisation.docker.enable = true;
  environment.etc = {
    ${composefile} = {
      text = ''
        #
        # WARNING: Make sure to use the docker-compose.yml of the current release:
        #
        # https://github.com/immich-app/immich/releases/latest/download/docker-compose.yml
        #
        # The compose file on main may not be compatible with the latest release.
        #

        name: immich

        services:
          immich-server:
            user: "${photos-uid-string}:${photos-gid-string}"
            container_name: immich_server
            image: ghcr.io/immich-app/immich-server:''${IMMICH_VERSION:-release}
            volumes:
              - ''${UPLOAD_LOCATION}:/usr/src/app/upload
              - /etc/localtime:/etc/localtime:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/SortedMedia/Sean:/usr/src/app/externalSean:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/SortedMedia/Sean&Poppy:/usr/src/app/externalSean&Poppy:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/SortedMedia/Sean&Dad:/usr/src/app/externalSean&Dad:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/SortedMedia/Family:/usr/src/app/externalFamily:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/SortedMedia/Family:/usr/src/app/dadExternalFamily:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/SortedMedia/Sean&Dad:/usr/src/app/dadExternalSean&Dad:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/SortedMedia/Dad:/usr/src/app/dadExternalDad:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/SortedMedia/Lauren&Mum&Dad:/usr/src/app/dadExternalLauren&Mum&Dad:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/filmScan2/:/usr/src/app/dadExternalFilmScan:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/Pics_unsorted_backup/:/usr/src/app/dadExternalPics_unsorted_backup:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/SortedMedia/Family:/usr/src/app/laurenExternalFamily:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/SortedMedia/Lauren:/usr/src/app/laurenExternalLauren:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/SortedMedia/Lauren&Mum:/usr/src/app/laurenExternalLauren&Mum:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/SortedMedia/Lauren&Mum&Dad:/usr/src/app/laurenExternalLauren&Mum&Dad:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/filmScan2/:/usr/src/app/laurenExternalFilmScan:ro
              - /home/sean/storage/PcBackup/LocalStorage/MyMedia/Pics_unsorted_backup/:/usr/src/app/laurenExternalPics_unsorted_backup:ro
            env_file:
              - ${config.sops.secrets."variables".path}
            ports:
              - 2283:3001
            depends_on:
              - redis
              - database
            restart: always

          immich-machine-learning:
            container_name: immich_machine_learning
            # For hardware acceleration, add one of -[armnn, cuda, openvino] to the image tag.
            # Example tag: ''${IMMICH_VERSION:-release}-cuda
            image: ghcr.io/immich-app/immich-machine-learning:''${IMMICH_VERSION:-release}
            # extends: # uncomment this section for hardware acceleration - see https://immich.app/docs/features/ml-hardware-acceleration
            #   file: hwaccel.ml.yml
            #   service: cpu # set to one of [armnn, cuda, openvino, openvino-wsl] for accelerated inference - use the `-wsl` version for WSL2 where applicable
            volumes:
              - model-cache:/cache
            env_file:
              - ${config.sops.secrets."variables".path}
            restart: always

          redis:
            container_name: immich_redis
            image: registry.hub.docker.com/library/redis:6.2-alpine@sha256:84882e87b54734154586e5f8abd4dce69fe7311315e2fc6d67c29614c8de2672
            restart: always

          database:
            container_name: immich_postgres
            image: registry.hub.docker.com/tensorchord/pgvecto-rs:pg14-v0.2.0@sha256:90724186f0a3517cf6914295b5ab410db9ce23190a2d9d0b9dd6463e3fa298f0
            environment:
              POSTGRES_PASSWORD: ''${DB_PASSWORD}
              POSTGRES_USER: ''${DB_USERNAME}
              POSTGRES_DB: ''${DB_DATABASE_NAME}
            volumes:
              - ''${DB_DATA_LOCATION}:/var/lib/postgresql/data
            restart: always

        volumes:
          model-cache:
      '';
    };
  };

  systemd.services."immish" = {
    description = "photo service";
    wantedBy = [ "multi-user.target" ];
    after = [ "docker.service" "docker.socket" ];
    requires = [ "docker.service" "docker.socket" ];

    serviceConfig = {
      ExecStart = "${pkgs.docker-compose}/bin/docker-compose -f /etc/${composefile} up";
      EnvironmentFile = config.sops.secrets."variables".path;
    };
    preStop = "${pkgs.docker-compose}/bin/docker-compose -f /etc/${composefile} down";
    reload = "${pkgs.docker-compose}/bin/docker-compose -f /etc/${composefile} restart";
  };
}
