{ pkgs, lib, ... }:
{
  imports = [
    ../../roles/sops.nix
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/nvme0n1";
  boot.loader.grub.useOSProber = true;

  networking = {
    hostName = "pc"; # Define your hostname.
  };

  networking.firewall.allowedTCPPorts = [
    8080 # scrutiny
    8385 # influxdb2 gui
    4894 # random for dev stuff
  ];

  environment.systemPackages = [ pkgs.zfs pkgs.rclone ];
  boot.supportedFilesystems = [ "zfs" "ntfs" ];
  networking.hostId = "007f0200";
  services.zfs.autoScrub.enable = true;
  services.zfs.autoScrub.interval = "monthly";
  services.zfs.autoSnapshot.enable = true;

  systemd.services.zfs-mount.enable = false; ## todo document

  sops.secrets.sean = {
    sopsFile = ./zfs-secrets.yaml;
  };
  sops.secrets.poppy = {
    sopsFile = ./zfs-secrets.yaml;
  };
  sops.secrets.photos = {
    sopsFile = ./zfs-secrets.yaml;
  };



  virtualisation.docker.enable = true;
  systemd.services."scrutiny" = {
    description = "scrutiny";
    wantedBy = [ "multi-user.target" ];
    after = [ "docker.service" "docker.socket" ];
    requires = [ "docker.service" "docker.socket" ];
    serviceConfig = {
      ExecStart = "${pkgs.docker}/bin/docker run --rm -p 8080:8080 -p 8087:8086   -v /etc/scrutiny:/opt/scrutiny/config   -v /etc/influxdb2:/opt/scrutiny/influxdb   -v /run/udev:/run/udev:ro   --cap-add SYS_RAWIO   --device=/dev/sda --device=/dev/sdb --device=/dev/sdc --device=/dev/sdd --device=/dev/sde --device=/dev/sdf   --name scrutiny   ghcr.io/analogj/scrutiny:master-omnibus";
    };
    preStop = "${pkgs.docker}/bin/docker stop scrutiny";
    # running with --rm so reload is a stop and remake
    reload = "${pkgs.docker}/bin/docker stop scrutiny && ${pkgs.docker}/bin/docker run --rm -p 8080:8080 -p 8086:8086   -v /etc/scrutiny:/opt/scrutiny/config   -v /etc/influxdb2:/opt/scrutiny/influxdb   -v /run/udev:/run/udev:ro   --cap-add SYS_RAWIO   --device=/dev/sda --device=/dev/sdb --device=/dev/sdc --device=/dev/sdd --device=/dev/sde --device=/dev/sdf   --name scrutiny   ghcr.io/analogj/scrutiny:master-omnibus";
  };




  programs.steam = {
    enable = true;
    remotePlay.openFirewall = false; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = false; # Open ports in the firewall for Source Dedicated Server
  };

  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "steam"
    "steam-original"
    "steam-run"
  ];


}
