{ config, pkgs, lib, ... }: {

  # add 1m filesystemmounted in ram

  fileSystems."/root/ramdisk" =
    {
      device = "tmpfs";
      fsType = "tmpfs";
      options = [ "size=1m" ];
    };


  imports = [
    # Include the results of the hardware scan.
    ../boards/rpi3.nix
    ../boards/rpi4-wifi.nix
  ];
  networking = {
    hostName = "rustah"; # Define your hostname.
  };
  #  hardware.enableAllFirmware = true;
  hardware.enableRedistributableFirmware = true;

  networking.wireless.networks = lib.mkForce {
    "Codethink Guest" = {
      extraConfig = "bssid_whitelist=4a:d9:e7:fd:b8:98\n";
      psk = "@GUEST_PASS@";
      priority = 10;
    };
  };
}
