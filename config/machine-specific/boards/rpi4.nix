{ pkgs, ... }:
{
  boot = {
    # tmpOnTmpfs = true; # Dont use this it makes /tmp a new partition, faily small aswell  (on pi) so easily fills up
    kernelPackages = pkgs.linuxPackages_rpi4;
    initrd.availableKernelModules = [ "usbhid" ];
    # ttyAMA0 is the serial console broken out to the GPIO
    kernelParams = [
      "8250.nr_uarts=1"
      "console=ttyAMA0,115200"
      "console=tty1"
      # A lot GUI programs need this, nearly all wayland applications
      "cma=128M"
    ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
      options = [ "noatime" ];
    };
  };

  # NixOS wants to enable GRUB by default
  boot.loader.grub.enable = false;
  # Enables the generation of /boot/extlinux/extlinux.conf
  boot.loader.generic-extlinux-compatible.enable = true;
  #  boot.kernelPackages = pkgs.linuxPackages_rpi4;
  #  boot.kernelPackages = pkgs.linuxPackages_latest;

  # Required for the Wireless firmware
  hardware.enableRedistributableFirmware = true;

  #  boot.loader.raspberryPi = {
  #    enable = true;
  #    version = 4;
  #    uboot.enable = true;
  #  };

  swapDevices = [{ device = "/swapfile"; size = 2048; }];


}
