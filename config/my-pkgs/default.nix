{ pkgs, ... }:
with pkgs; {
  required-scripts = callPackage ./required-scripts { inherit pkgs; };
  scripts = callPackage ./scripts { inherit pkgs; };
  boat-coordinator = callPackage ./boat-coordinator { inherit pkgs; };
  victron-influx-importer = callPackage ./victron-influx-importer { inherit pkgs; };
  esp32-influx-importer = callPackage ./esp32-influx-importer { inherit pkgs; };
  mqtt-watchman = callPackage ./mqtt-watchman { inherit pkgs; };
  mqtt-watchman-snork = callPackage ./mqtt-watchman-snork { inherit pkgs; };
  leftHandMouseRemap = callPackage ./left-hand-mouse-remap { inherit pkgs; };
}
