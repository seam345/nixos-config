{ pkgs, stdenv, fetchFromGitLab, ... }: pkgs.rustPlatform.buildRustPackage rec {
  pname = "left-hand-mouse-remap";
  version = "1.0.0.debug";

  src = fetchFromGitLab {
    domain = "gitlab.com";
    owner = "seam345";
    repo = pname;
    rev = version;
    sha256 = "sha256-+gCJ6b3y30K8EfFUajRS/BGiykhm7fHWqFbCTsm06Q4=";
  };

  cargoSha256 = "sha256-NC+uvl+XXcWm+Tkjnnx3XpZ9O6uTePlL6uOj0PCOtQ8=";
}