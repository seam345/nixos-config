{ pkgs, stdenv, fetchFromGitLab, ... }: pkgs.rustPlatform.buildRustPackage rec {
  pname = "mqtt-watchman";
  version = "v0.4";

  src = fetchFromGitLab {
      domain = "gitlab.com";
      owner = "seam345";
      repo = pname;
      rev = "c498760";
      sha256 = "sha256-Y3FnMFaqwKgl3zFp2BMBCD2PdqlvAaCnzM9TmzY8Ews=";
    };

  cargoSha256 = "sha256-/kF+d0SDZbzgiCRiOJ/Ezsp03+rLDaCbk1NDPPiu8jQ=";

  # compile time env variables
#  INFLUXDB_HOST="http://localhost";
#  INFLUXDB_BUCKET="N/A";
#  INFLUXDB_ORG="N/A";
  MQTT_ANNOUNCE_NAME = "mqtt_watchman";
  MQTT_HOST = "localhost";


  # todo learn how to make the above options so there are not 2 packages
}