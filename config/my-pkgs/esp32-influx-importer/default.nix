{ pkgs, stdenv, fetchFromGitLab, ... }: pkgs.rustPlatform.buildRustPackage rec {
  pname = "influx-esp";
  version = "v0.1";

  src = fetchFromGitLab {
            domain = "gitlab.com";
            owner = "seam345";
            repo = pname;
            rev = "587bd08";
            sha256 = "sha256-3AYi99WEqg5ujLWDGpIbVZKpSiSt0FaV4FYAnpKw9JY=";
          };

  cargoSha256 = "sha256-SeyWTyBG63nIl+T/0t9Vla5G1S3Z2TjxmaywEdzsQVQ=";

  # compile time env variables
  INFLUXDB_HOST="http://100.89.209.13:8086";
  INFLUXDB_BUCKET="Sensors";
  INFLUXDB_ORG="Boat";
  MQTT_ANNOUNCE_NAME = "influx-esp-ingester";
  MQTT_HOST = "100.96.69.67";
}