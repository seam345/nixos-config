{ pkgs, stdenv, fetchFromGitLab, ... }: pkgs.rustPlatform.buildRustPackage rec {
  pname = "mqtt-watchman";
  version = "v0.4";

  src = fetchFromGitLab {
      domain = "gitlab.com";
      owner = "seam345";
      repo = pname;
      rev = "c498760";
      sha256 = "sha256-Y3FnMFaqwKgl3zFp2BMBCD2PdqlvAaCnzM9TmzY8Ews=";
    };

  cargoSha256 = "sha256-/kF+d0SDZbzgiCRiOJ/Ezsp03+rLDaCbk1NDPPiu8jQ=";

  # compile time env variables
  INFLUXDB_HOST="http://100.89.209.13:8086";
#  INFLUXDB_HOST="http://${import ../../vars/ips/boat-athena.nix}:8086";
  INFLUXDB_BUCKET="Logs";
  INFLUXDB_ORG="Boat";
  MQTT_ANNOUNCE_NAME = "mqtt_watchman";
  MQTT_HOST = "100.89.209.13";
#  MQTT_HOST = "${import ../../vars/ips/boat-athena.nix}";

  RUSTFLAGS="--cfg influx_log=\"true\"";
}