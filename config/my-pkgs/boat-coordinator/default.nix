{ pkgs, ... }:
with pkgs; {
#  frontroom-down-lights = callPackage ./frontroom-down-lights { inherit pkgs; };
#  dinette-down-lights = callPackage ./dinette-down-lights { inherit pkgs; };
#  bathroom-down-lights = callPackage ./bathroom-down-lights { inherit pkgs; };
#  kitchen-down-lights = callPackage ./kitchen-down-lights { inherit pkgs; };
#  bedroom-down-lights = callPackage ./bedroom-down-lights { inherit pkgs; };
#  engineroom-down-lights = callPackage ./engineroom-down-lights { inherit pkgs; };
  watchman = callPackage ./watchman { inherit pkgs; };
  watchman-boat = callPackage ./watchman/boat.nix { inherit pkgs; };
  washing-log = callPackage ./washing-log { inherit pkgs; };
#  wardrobe = callPackage ./wardrobe { inherit pkgs; };

  frontroom-down-lights = callPackage  ./skipper-package { 
    binary = "frontroom-down-lights"; 
    versionTag= "v0.14";
    cargoSha = "sha256-IQEMCxfnGnEvrZTZ1ZHkDQ1c6BJRd+BqDFapB4T38O4=";
    mqttAnnounceName="frontroom-down-lights"; 
    logInflux=true;
    appName = "frontroom-down-lights";
  };
  dinette-down-lights = callPackage ./skipper-package { 
    binary = "dinette-down-lights"; 
    versionTag= "v0.11";
    cargoSha = "sha256-RKyi83cuvcXS6vYbUDcmgKPfAPnfRsOZ+EWjeuE5bZY=";
    mqttAnnounceName="dinette-down-lights"; 
    logInflux=true;
    appName = "dinette-down-lights";
  };
  kitchen-down-lights = callPackage  ./skipper-package { 
    binary = "kitchen-lights"; 
    versionTag= "v0.8";
    cargoSha = "sha256-+mz8cb8ecGFBXWEZmhA4WN/hyA5X/MzU2ERiOrhw4fc=";
    mqttAnnounceName="kitchen-lights"; 
    logInflux=true;
    appName = "kitchen-lights";
  };
  bathroom-down-lights = callPackage  ./skipper-package { 
    binary = "bathroom-down-lights";
    versionTag= "v0.12";
    cargoSha = "sha256-iN1AURiG22DS3keNfQNZwpRPTtygVDwUfDddC54GddY=";
    mqttAnnounceName="bathroom-down-lights";
    logInflux=true;
    appName = "bathroom-down-lights";
  };
  bedroom-down-lights = callPackage ./skipper-package { 
    binary = "bedroom-down-lights"; 
    versionTag= "v0.8";
    cargoSha = "sha256-b+vwzyS62R9ttcPhYcWy4mDkS5TjUPHoYo/iOBaqewQ=";
    mqttAnnounceName="bedroom-down-lights";
    logInflux=true;
    appName = "bedroom-down-lights";
  };
  engineroom-down-lights = callPackage  ./skipper-package {
    binary = "engineroom-down-lights";
    versionTag= "v0.7";
    cargoSha = "sha256-QQvhKsmu4WSuvw87raUEdXbIiSOJHHD0jMx8iiE2OgI=";
    mqttAnnounceName="engineroom-down-lights";
    logInflux=true;
    appName = "engineromm-down-lights";
  };
  wardrobe = callPackage  ./skipper-package {
    binary = "wardrobe";
    versionTag= "v0.2";
    cargoSha = "sha256-z7+y2m3LaPhdFK/h9UfcDS39Q7My6u5RVd8o5a/2oJw=";
    mqttAnnounceName="wardrobe";
    logInflux=true;
    appName = "wardrobe";
  };
  boat-occupied = callPackage  ./skipper-package {
    binary = "boat_occupied";
    versionTag= "v0.2";
    cargoSha = "sha256-w0T6W33hsu/XrlnShJUAuA+g9kyt2R39Hth9+VlT09g=";
    mqttAnnounceName="boat-occupied";
    logInflux=true;
    appName="boat-occupied";
  };
  boat-occupied-lights = callPackage  ./skipper-package {
    binary = "boat_occupied_lights";
    versionTag= "v0.1";
    cargoSha = "sha256-+q+OJz0M92Asu0yhAOen1RDqdmB34spmzYOmDEaiEI0=";
    mqttAnnounceName="boat-occupied-lights";
    logInflux=true;
    appName="boat-occupied-lights";
  };
  bedroom-tap-dial = callPackage  ./skipper-package {
      binary = "bedroom-tap-dial";
      versionTag= "v0.1";
      cargoSha = "sha256-hroGgLtBaaVKrgPMQhbFtgkCzWQLpOofKmdD+JyoOs4=";
      mqttAnnounceName="bedroom-tap-dial";
      logInflux=true;
      appName="bedroom-tap-dial";
    };
#  pushup-log = callPackage  ./skipper-package { binary = "pushup-logger"; versionTag= "v0.1"; cargoSha = "sha256-1VChDpZJrdTKwfdT0kcoynXaugQoJw6A+d3JjUffjSE="; mqttAnnounceName="pushup-log";};
#  wash-counter = callPackage  ./skipper-package { binary = "wash_counter"; versionTag= "v0.1"; cargoSha = "sha256-TpyAgyZIEFlK16YNBy/oCBjAA1l/h6MC3Q3lG3zgRIQ=";mqttAnnounceName="wash-counter";};
}
