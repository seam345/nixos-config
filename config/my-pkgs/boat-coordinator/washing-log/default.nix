

{ pkgs, stdenv, ... }: pkgs.rustPlatform.buildRustPackage rec {
  pname = "wash_counter";
  version = "v0.4";

  src = builtins.fetchTarball
    {
      url = "https://gitlab.com/seam345/skipper/-/archive/${pname}-${version}/${pname}-${version}.tar.gz";
    };
  cargoSha256 = "sha256-UPSA3cD+weJJb16xAOofDAy8UPoXk86qblU0JXKIxQM=";
  cargoBuildFlags = "--bin ${pname}";
  cargoTestFlags = "--bin ${pname}";


  MQTT_ANNOUNCE_NAME = "mqtt_wash-counter";
  MQTT_HOST = "localhost";
}

