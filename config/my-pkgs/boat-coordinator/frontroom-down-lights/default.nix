{ pkgs, stdenv, ... }: pkgs.rustPlatform.buildRustPackage rec {
  pname = "frontroom-down-lights";
  version = "v0.12";

  src = builtins.fetchTarball "https://gitlab.com/seam345/skipper/-/archive/frontroom-down-lights-${version}/skipper-frontroom-down-lights-${version}.tar.gz";
  # I need to sort out closding with light state
  cargoSha256 = "sha256-U2AI6tQPQhvParA4yt04MR5h82W+reF1O/ipOeOxidw=";
  cargoBuildFlags = "--bin frontroom-down-lights";
  cargoTestFlags = "--bin frontroom-down-lights";

  MQTT_HOST = "100.96.69.67";
  MQTT_ANNOUNCE_NAME = "frontroom-downlight-switch";

  INFLUXDB_HOST="http://100.89.209.13:8086";
  INFLUXDB_BUCKET="Logs";
  INFLUXDB_ORG="Boat";
  RUSTFLAGS="--cfg influx_log=\"true\"";
}

