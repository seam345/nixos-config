{ pkgs, stdenv, ... }: pkgs.rustPlatform.buildRustPackage rec {
  pname = "kitchen-down-lights";
  version = "v0.6";

  src = builtins.fetchTarball "https://gitlab.com/seam345/skipper/-/archive/kitchen-lights-${version}/skipper-kitchen-down-lights-${version}.tar.gz";
  cargoSha256 = "";
  cargoBuildFlags = "--bin kitchen-lights";
  cargoTestFlags = "--bin kitchen-lights";

  MQTT_HOST = "100.96.69.67";
  MQTT_ANNOUNCE_NAME = "kitchen-downlight-switch";

  INFLUXDB_HOST="http://100.89.209.13:8086";
  INFLUXDB_BUCKET="Logs";
  INFLUXDB_ORG="Boat";
  RUSTFLAGS="--cfg influx_log=\"true\"";

}

