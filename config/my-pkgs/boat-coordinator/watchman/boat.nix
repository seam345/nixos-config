{ pkgs, stdenv, ... }: pkgs.rustPlatform.buildRustPackage rec {
  pname = "watchman";
  version = "v1.1";

  src = builtins.fetchTarball
    {
      url = "https://gitlab.com/seam345/skipper/-/archive/${pname}-${version}/${pname}-${version}.tar.gz";
    };
  cargoSha256 = "sha256-w/py+PFEB+BPPfylvRs3i9G5NnZ4+tTrB0cZSfBG444=";
  cargoBuildFlags = "--bin ${pname}";
  cargoTestFlags = "--bin ${pname}";


#todo make these overidable
  MQTT_ANNOUNCE_NAME = "mqtt_watchman";
#  MQTT_HOST = "100.96.69.67";
  MQTT_HOST = "${import ../../../vars/ips/boat-athena.nix}";



#  LOG_INFLUXDB_HOST = "http://100.89.209.13:8086";
  LOG_INFLUXDB_HOST="http://${import ../../../vars/ips/boat-athena.nix}:8086";
    LOG_INFLUXDB_BUCKET = "Logs";
    LOG_INFLUXDB_ORG = "Boat";
    APP_NAME = "watchman"; # usually same as announce name but is techially separate and used for logging

    RUSTFLAGS= "--cfg influx_log=\"true\"";
}

