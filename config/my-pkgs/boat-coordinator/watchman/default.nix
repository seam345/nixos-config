{ pkgs, stdenv, ... }: pkgs.rustPlatform.buildRustPackage rec {
  pname = "watchman";
  version = "v1.1";

  src = builtins.fetchTarball
    {
      url = "https://gitlab.com/seam345/skipper/-/archive/${pname}-${version}/${pname}-${version}.tar.gz";
    };
  cargoSha256 = "sha256-w/py+PFEB+BPPfylvRs3i9G5NnZ4+tTrB0cZSfBG444=";
  cargoBuildFlags = "--bin ${pname}";
  cargoTestFlags = "--bin ${pname}";


#todo make these overidable
  MQTT_ANNOUNCE_NAME = "mqtt_watchman";
  MQTT_HOST = "localhost";
}

