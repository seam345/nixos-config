{
  pkgs,
  stdenv,
  binary,
  cargoSha,
  versionTag,
  mqttAnnounceName? "skipper-test",
  logInflux? false,
  mqttHost? "${import ../../../vars/ips/boat-athena.nix}",
  influxdbOrg? "test",
  influxdbBucket? "Test",
  influxdbHost? "http://${import ../../../vars/ips/boat-athena.nix}:8086",
  appName? "skipper-test",
  logInfluxdbOrg? "Boat",
  logInfluxdbBucket? "Logs",
  logInfluxdbHost? "http://${import ../../../vars/ips/boat-athena.nix}:8086",
  ...
}:
pkgs.rustPlatform.buildRustPackage rec {
  pname = "skipper-${binary}";
  version = "${versionTag}";

  src = builtins.fetchTarball "https://gitlab.com/seam345/skipper/-/archive/${binary}-${versionTag}/skipper-${binary}-${versionTag}.tar.gz";

  cargoSha256 = "${cargoSha}";
  cargoBuildFlags = "--bin ${binary}";
  cargoTestFlags = "--bin ${binary}";


  LOG_INFLUXDB_HOST = logInfluxdbHost;
  LOG_INFLUXDB_BUCKET = logInfluxdbBucket;
  LOG_INFLUXDB_ORG = logInfluxdbOrg;
  APP_NAME = appName; # usually same as announce name but is techially separate and used for logging

  # the bellow influx variables are where to send non logging influx info
  INFLUXDB_HOST = influxdbHost;
  INFLUXDB_BUCKET = influxdbBucket;
  INFLUXDB_ORG = influxdbOrg;

  MQTT_HOST = mqttHost;
  MQTT_ANNOUNCE_NAME =  mqttAnnounceName;

  RUSTFLAGS= if logInflux then "--cfg influx_log=\"true\"" else "";

}
