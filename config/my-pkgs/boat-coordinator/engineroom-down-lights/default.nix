{ pkgs, stdenv, ... }: pkgs.rustPlatform.buildRustPackage rec {
  pname = "engineroom-down-lights";
  version = "v0.5";

  src = builtins.fetchTarball
    {
      url = "https://gitlab.com/seam345/skipper/-/archive/engineroom-down-lights-${version}/skipper-engineroom-down-lights-${version}.tar.gz";
    };
  cargoSha256 = "sha256-rePkAyRmAj9zGiCY9O8A8xxJqub+QsiijKPlR8+v63k=";
  cargoBuildFlags = "--bin engineroom-down-lights"; # anoying typo
  cargoTestFlags = "--bin engineroom-down-lights"; # anoying typo

  MQTT_HOST = "100.96.69.67";
    MQTT_ANNOUNCE_NAME = "bathroom-downlight-switch";

    INFLUXDB_HOST="http://100.89.209.13:8086";
    INFLUXDB_BUCKET="Logs";
    INFLUXDB_ORG="Boat";
    RUSTFLAGS="--cfg influx_log=\"true\"";
}

