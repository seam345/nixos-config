{ pkgs, stdenv, ... }: pkgs.rustPlatform.buildRustPackage rec {
  pname = "bedroom-down-lights";
  version = "v0.6";

  src = builtins.fetchTarball "https://gitlab.com/seam345/skipper/-/archive/bedroom-down-lights-${version}/bedroom-down-lights-${version}.tar.gz";
  cargoSha256 = "sha256-jzgWHcYNw3XMzengH9QFRJBJSsbNDcZ6VbdC8XbFiT4=";
  cargoBuildFlags = "--bin bedroom-down-lights";
  cargoTestFlags = "--bin bedroom-down-lights";

  MQTT_HOST = "100.96.69.67";
  MQTT_ANNOUNCE_NAME = "bathroom-downlight-switch";

  INFLUXDB_HOST="http://100.89.209.13:8086";
  INFLUXDB_BUCKET="Logs";
  INFLUXDB_ORG="Boat";
  RUSTFLAGS="--cfg influx_log=\"true\"";

}

