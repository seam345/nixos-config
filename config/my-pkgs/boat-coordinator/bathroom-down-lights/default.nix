{ pkgs, stdenv, ... }: pkgs.rustPlatform.buildRustPackage rec {
  pname = "bathroom-down-lights";
  version = "v0.10";

  src = builtins.fetchTarball "https://gitlab.com/seam345/skipper/-/archive/bathroom-down-lights-${version}/skipper-bathroom-down-lights-${version}.tar.gz";

  cargoSha256 = "sha256-rg8zOM49LZOghI0nIh8Ur0eJAbFiMMA/4/jGsicROvk=";
  cargoBuildFlags = "--bin bathroom-down-lights";
  cargoTestFlags = "--bin bathroom-down-lights";

  MQTT_HOST = "100.96.69.67";
  MQTT_ANNOUNCE_NAME = "bathroom-downlight-switch";

  INFLUXDB_HOST="http://100.89.209.13:8086";
  INFLUXDB_BUCKET="Logs";
  INFLUXDB_ORG="Boat";
  RUSTFLAGS="--cfg influx_log=\"true\"";
}

