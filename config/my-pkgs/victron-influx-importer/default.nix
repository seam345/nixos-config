{ pkgs, stdenv, fetchFromGitLab, ... }: pkgs.rustPlatform.buildRustPackage rec {
  pname = "victron-influx-importer";
  version = "v0.3";

  src = fetchFromGitLab {
            domain = "gitlab.com";
            owner = "seam345";
            repo = pname;
            rev = "84edc2a";
            sha256 = "sha256-hJamIY5hIggm0n8+3S+elB9DP22+O9cyb0HIriXuFSM=";
          };

  cargoSha256 = "sha256-BIxnMKS/ZKwU6nF4pYi5TnaCBWK0hhAbe0zPLQnnHao=";

  # compile time env variables
  INFLUXDB_HOST="http://100.89.209.13:8086";
  INFLUXDB_BUCKET="Power";
  INFLUXDB_ORG="Boat";
  MQTT_ANNOUNCE_NAME = "influx-solar-power-ingester";
  MQTT_HOST = "100.82.216.40";
}