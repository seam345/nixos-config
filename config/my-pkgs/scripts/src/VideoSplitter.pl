#!/usr/bin/env perl
use strict;
use warnings FATAL => 'all';
use Cwd;



my @files = <*>;
foreach my $file (@files) {
    if ($file =~ /.*\.m4v$/ ){
        my $fileName = $file =~ s/.m4v//r;
        `/home/sean/.local/bin/scenedetect --input $file -o $fileName detect-content -t 65 list-scenes split-video`;
        my $dir = `pwd`;
        chomp($dir);
        my $newDir = $dir . "/" . $fileName;
        chdir($newDir);
        {
            my @files2 = <*>;
            foreach my $file2 (@files2) {
                if ($file2 =~ /.*\.mp4/ ){
                    my $fileName2 = $file2 =~ s/.mp4//r;
                    print $file2 . "\n";
                    `/home/sean/.local/bin/scenedetect --input $file2 -o $fileName2 detect-content -t 50 list-scenes split-video`;
                    my $dir2 = `pwd`;
                    chomp($dir2);
                    my $newDir2 = $dir2 . "/" . $fileName2;
                    chdir($newDir2);
                    {

                        my @files3 = <*>;
                        foreach my $file3 (@files3) {
                            if ($file3 =~ /.*\.mp4/ ){
                                my $fileName3 = $file3 =~ s/.mp4//r;
                                `/home/sean/.local/bin/scenedetect --input $file3 -o $fileName3 detect-content -t 45 list-scenes  split-video`;
                                my $dir3 = `pwd`;
                                chomp($dir3);
                                my $newDir3 = $dir3 . "/" . $fileName3;
                                chdir($newDir3);
                                {

                                    my @files4 = <*>;
                                    foreach my $file4 (@files4) {
                                        if ($file4 =~ /.*\.mp4/ ){
                                            my $fileName4 = $file4 =~ s/.mp4//r;
                                            `/home/sean/.local/bin/scenedetect --input $file4 -o $fileName4 detect-content -t 40 list-scenes  split-video`;
                                            # `cd $fileName`;

                                            # `cd ..`
                                        }
                                    }
                                }
                                chdir($dir3)
                            }
                        }
                    }
                    chdir($dir2)
                }
            }
        }
        chdir($dir)
    }
}