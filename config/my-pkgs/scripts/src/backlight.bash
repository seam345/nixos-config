#!/bin/bash
#this script shall set the backlight of the laptop
#the dollar sign with the 1 is the variable typed in from command line

if [ $# -gt 0 ]
then sudo tee /sys/class/backlight/intel_backlight/brightness <<< $1
else echo please enter the number for brightness after the script
fi
