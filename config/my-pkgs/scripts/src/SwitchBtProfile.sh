

# bellow was a merge of sed -n '/<pattern>/{x;p;d;}; x' << this prints the previous line from a match
# and this one sed -ne '/<pattern>/,$ p' << this prints out everything from bellow the pattern

# set -x

btProfile=$(pacmd list-cards | sed -ne '/bluez_card.4C_87_5D_28_BA_47/,${x;p;d;}; x') 

indexLine=$(echo "$btProfile" | grep index:)
index=$(echo $indexLine | tr -dc [:digit:])

activeProfileLine=$(echo "$btProfile" | grep "active profile:")
activeProfile=$(echo $activeProfileLine |  sed -e 's/^.*<\(.*\)>/\1/')

headsetMode=headset_head_unit
normalMode=a2dp_sink

if [[ "$activeProfile" == "$normalMode" ]]
then
    pacmd set-card-profile $index $headsetMode
else
    pacmd set-card-profile $index $normalMode
fi



