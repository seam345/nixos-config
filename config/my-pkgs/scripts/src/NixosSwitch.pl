#!/usr/bin/env perl
use strict;
use warnings FATAL => 'all';

# give building a try before attempting to switch
`nixos-rebuild dry-build`;
if ($?) {
    die;
}
my ($name) = @ARGV;

if (not defined $name) {
  die "Need a name for the nixos version\n";
}

my $hostname = `hostname`;
chomp($hostname);
my $dateString = `date +%Y-%m-%d_%H%M`;
chomp($dateString);
my $tag = $hostname . "_" . $dateString . "_" . $name;
my $nixProfileString = $dateString . "_" . $name;
print("\n\nPlease tap yubikey :p\n\n");
print(`cd /etc/nixos && git add . && git commit --allow-empty -m "Nixos rebuild $hostname" && git tag $tag && git reset HEAD^1 && git push --tags`);
if ($?) {
    die;
}
sleep(7); # needed for yubikey to be ready again
print("\n\nPlease tap yubikey :p\n\n");
`sudo nixos-rebuild  switch -p "$nixProfileString"`;
