#!/usr/bin/env perl
use strict;
use warnings FATAL => 'all';
use List::Util qw(first);


my $currentWorkspace = `i3-msg -t get_workspaces |  jq -r ".[] | select(.focused==true).name"`;
my $currentOutput = `i3-msg -t get_workspaces |  jq ".[] | select(.focused==true).output"`;
my @curOutputWorkspaceNames = `i3-msg -t get_workspaces |  jq -r '.[] | select(.output==$currentOutput).name' | grep -v hidden`;
my @workspaceNames = `i3-msg -t get_workspaces |  jq -r '.[] | .name' | grep -v hidden | sort -n`;



if ($curOutputWorkspaceNames[-1] =~ /$currentWorkspace/)
{
  my $incrementWorkspace = int($workspaceNames[-1]) + 1;
  `i3-msg move container to workspace $incrementWorkspace`;
  `i3-msg workspace $incrementWorkspace`;
}else
{
	my $idx = first { $curOutputWorkspaceNames[$_] eq $currentWorkspace } 0..$#curOutputWorkspaceNames;
  `i3-msg move container to workspace $curOutputWorkspaceNames[int($idx) + 1]`;
  `i3-msg workspace $curOutputWorkspaceNames[int($idx) + 1]`;
}
