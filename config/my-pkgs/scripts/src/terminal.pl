#!/usr/bin/env perl
use strict;
use warnings FATAL => 'all';

my @potentialIDS = split /\n/, `xdotool search --class Mate-terminal`;
my $file = '/home/sean/teminalOpen.flag';

if (scalar(@potentialIDS) > 0)
{
    if (-e $file)
    {
        for my $id (@potentialIDS)
        {
            `i3-msg  [class="Mate-terminal"] move to workspace current; i3-msg [class="Mate-terminal"] focus`;
        }
        `rm $file`;
    } else
    {
        for my $id (@potentialIDS)
        {
            `i3-msg  [class="Mate-terminal"] move to workspace hiddenTerminal`;
        }
        `touch $file`;
    }
} else
{
    `i3-msg exec mate-terminal`;

    `touch $file`;
}
