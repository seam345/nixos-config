

# to cleanup runners (order matters as the volumes hold onto nix references)
# docker system prune --volumes -af
# nix-collect-garbage -d
# nix-store --optimise # not needed as often

{ config, lib, pkgs, ... }:
{
  imports = [
    # Include the results of the hardware scan.
    ./sops.nix
  ];
  # I think the bellow two statements make the file in /run/secrets
  sops.secrets.gitlab-runner-token-file = {
    sopsFile = ./gitlab-runner-secrets.yaml;
  };
  sops.secrets.nixos-config-token-file = {
    sopsFile = ./gitlab-runner-secrets.yaml;
  };
  sops.secrets.markdown-notes-token-file = {
    sopsFile = ./gitlab-runner-secrets.yaml;
  };
  sops.secrets.my-website-token-file = {
    sopsFile = ./gitlab-runner-secrets.yaml;
  };
  boot.kernel.sysctl."net.ipv4.ip_forward" = true; # 1
  virtualisation.docker.enable = true;

  services.gitlab-runner = {
    clear-docker-cache.enable = true;
    clear-docker-cache.flags = [ "prune" ];
    enable = true;
    services = {
      # runner for building in docker via host's nix-daemon
      # nix store will be readable in runner, might be insecure
      nix = with lib;{
        # File should contain at least these two variables:
        # `CI_SERVER_URL`
        # `REGISTRATION_TOKEN`
        registrationConfigFile = toString config.sops.secrets.gitlab-runner-token-file.path; # 2
        dockerImage = "alpine";
        dockerVolumes = [
          "/nix/store:/nix/store:ro"
          "/nix/var/nix/db:/nix/var/nix/db:ro"
          "/nix/var/nix/daemon-socket:/nix/var/nix/daemon-socket:ro"
        ];
        dockerDisableCache = true;
        preBuildScript = pkgs.writeScript "setup-container" ''
          mkdir -p -m 0755 /nix/var/log/nix/drvs
          mkdir -p -m 0755 /nix/var/nix/gcroots
          mkdir -p -m 0755 /nix/var/nix/profiles
          mkdir -p -m 0755 /nix/var/nix/temproots
          mkdir -p -m 0755 /nix/var/nix/userpool
          mkdir -p -m 1777 /nix/var/nix/gcroots/per-user
          mkdir -p -m 1777 /nix/var/nix/profiles/per-user
          mkdir -p -m 0755 /nix/var/nix/profiles/per-user/root
          mkdir -p -m 0700 "$HOME/.nix-defexpr"
          . ${pkgs.nix}/etc/profile.d/nix.sh
          ${pkgs.nix}/bin/nix-channel --add https://github.com/seam345/nixpkgs/archive/sean/my-branch.tar.gz nixpkgs # 3
          ${pkgs.nix}/bin/nix-channel --update nixpkgs
          ${pkgs.nix}/bin/nix-env -i ${concatStringsSep " " (with pkgs; [ nix cacert git openssh ])}
        '';
        environmentVariables = {
          ENV = "/etc/profile";
          USER = "root";
          NIX_REMOTE = "daemon";
          PATH = "/nix/var/nix/profiles/default/bin:/nix/var/nix/profiles/default/sbin:/bin:/sbin:/usr/bin:/usr/sbin";
          NIX_SSL_CERT_FILE = "/nix/var/nix/profiles/default/etc/ssl/certs/ca-bundle.crt";
        };
        tagList = [ "nix" "arm" ];
      };

      notnix = with lib;{
        registrationConfigFile = toString config.sops.secrets.gitlab-runner-token-file.path; # 2
        dockerImage = "alpine";
        #        dockerAllowedImages = [ "alpine:latest" "rust:buster" "debian:buster" "perl:5-buster" "rust:latest" "rustlang/rust:nightly" "rust:slim" "rustlang/rust:nightly-slim" ];
        tagList = [ "nas" "arm" "pi" ];
        runUntagged = true;
      };

      notnix-public = with lib;{
        registrationConfigFile = toString config.sops.secrets.nixos-config-token-file.path; # 2
        dockerImage = "alpine";
        protected = true; # public repo only run on trusted branches
        #        dockerAllowedImages = [ "alpine:latest" "rust:buster" "debian:buster" "perl:5-buster" "rust:latest" "rustlang/rust:nightly" "rust:slim" "rustlang/rust:nightly-slim" ];
        tagList = [ "nas" "arm" "pi" ];
        runUntagged = true;
      };

      nix-markdown-notes = with lib;{
        executor = "shell";
        registrationConfigFile = toString config.sops.secrets.markdown-notes-token-file.path; # 2
        tagList = [ "nix" "arm" ];
        runUntagged = true;
      };

      my-website = with lib;{
              executor = "shell";
              registrationConfigFile = toString config.sops.secrets.my-website-token-file.path; # 2
#              tagList = [ "nix" "arm" ];
#              runUntagged = true;
            };
    };
  };
}
