{
  imports = [
    ./home-manager.nix
  ];
  services.syncthing.enable = true;
  services.syncthing.systemService = false;
}