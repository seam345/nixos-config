{ pkgs, lib, config, ... }:
let
  composefile = "zigbee-docker-compose.yaml";
in
{
  services.zigbee2mqtt.enable = true;
  services.zigbee2mqtt.settings = {
    homeassistant =  false;
    permit_join = true;
    mqtt = {
      base_topic = "zigbee2mqtt";
      server = "mqtt://localhost:1883";
    };
    serial = {
      port=  "/dev/ttyUSB0";
    };
    frontend = {
      port = 8080;
    };
    advanced = {
      pan_id=  51209;
      ext_pan_id=  [ 223 224 72 43 229 207 107 165];
      homeassistant_legacy_entity_attributes = false;
      legacy_api = false;
      legacy_availability_payload = false;
      transmit_power = 20;
      last_seen = "ISO_8601";
    };
    device_options = {
      legacy = false;
    };
    groups = "groups.yaml";
    devices = "devices.yaml";
  };

  services.mosquitto = {
    enable = true;
    listeners = [
      {
        acl = [ "pattern readwrite #" ];
        omitPasswordAuth = true;
        settings.allow_anonymous = true;
      }
    ];
  };

  networking.firewall = {
    allowedTCPPorts = [
      1883 # mosquitto
      8080 # zigbee
    ];
  };
}
