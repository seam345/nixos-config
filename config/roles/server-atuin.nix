{ config, pkgs, ... }:
{
  services.atuin.enable = true;
  services.atuin.openFirewall = true;
  services.atuin.host = "${import ../vars/ips/pc.nix}"; # used to work over tailscale
  # open registration only needed when running atuin  register -u <USERNAME> -e <EMAIL>
  # which is not needed for new machines they only need to run atuin login! With the same encryption key
  # services.atuin.openRegistration = true;
}