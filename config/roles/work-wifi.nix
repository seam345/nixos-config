{ config, pkgs, ... }: {
  imports = [
    ./sops.nix
  ];
  sops.secrets.networking-work-env-file = {
    sopsFile = ./work-wifi-secrets.yaml;
  };

  networking.wireless.environmentFile = config.sops.secrets.networking-work-env-file.path;
  networking.wireless.enable = true;
  networking.wireless.userControlled.enable = true;
  # todo I need to confirm if this is overriding or adding to the wireless networks
  networking.wireless.networks = {
    "Codethink" = {
      priority = 10;
      authProtocols = [ "WPA-EAP" ];
      auth = ''
        eap=PEAP
        identity="@CODETHINK_IDENTITY@"
        password="@CODETHINK_PASSWORD@"
      '';
    };
    "tranquility" = {
      psk = "@TRANQUILITY_PASS@";
    };
    "BTHub5-SHG7" = {
      psk = "@BTHUB5_SHG7_PASS@";
    };
    "PLUSNET-M63K" = {
      psk = "@PLUSNET_M63K@";
    };
    "VM3728053" = {
      # authProtocols= ["SAE"];
      auth = ''
        key_mgmt=SAE
        sae_password="@VM3728053@"
        ieee80211w=2
      '';
    };
  };
}
