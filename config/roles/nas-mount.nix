{ pkgs, lib, config, ... }:
let
  nasmount = "/mnt/nas";
in
{
  systemd.services."nas-mount" = {
    description = "mount for nas";
    wantedBy = [ "multi-user.target" ];
    after = [ "tailscaled.service" ];
    requires = [ "tailscaled.service" ];
    serviceConfig = {
      ExecStart =
        let
          options = [
            "allow_other"
            "IdentityFile=/home/sean/.ssh/id_rsa"
            "IdentityFile=/home/sean/.ssh/id_ed25519"
          ];
        in
        "${pkgs.sshfs-fuse}/bin/sshfs  data@${import ./../vars/ips/nas.nix}:/data ${nasmount} -f "
        + lib.concatMapStringsSep " " (opt: "-o ${opt}") options;
      ExecStopPost = "-${pkgs.fuse}/bin/fusermount -u ${nasmount}";
      KillMode = "process";
      Restart = "on-failure";
    };
  };

}


