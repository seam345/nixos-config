

{ config, pkgs, lib, ... }:
let
  myXmodmap = pkgs.writeText "xkb-layout" ''
    keycode 107 = Super_R NoSymbol Super_R
  '';
  importJson = (import <nixpkgs> { }).lib.importJSON;
  home-manager = builtins.fetchGit {
    # update with: nix-shell -p nix-prefetch-git --command 'nix-prefetch-git   --url https://github.com/nix-community/home-manager   --rev refs/heads/master   > config/channels/home-manager.json'
    rev = (importJson ../channels/home-manager.json).rev;
    url = (importJson ../channels/home-manager.json).url;
    allRefs = true;
  };
in
{

  imports = [
    (import "${home-manager}/nixos")
  ];
}