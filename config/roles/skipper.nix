{ pkgs, config, ... }:
{
  imports = [
    ./sops.nix
  ];
#  sops
  sops.secrets.influx-log-token = {
      sopsFile = ./skipper-secrets.yaml;
    };

  nixpkgs.overlays = [ (self: super: { local = import ./my-pkgs { pkgs = super; }; }) ];
  systemd.services."frontroom-down-lights" = {
    enable = true;
    environment = {
      LOG_TOKEN_FILE = config.sops.secrets.influx-log-token.path;
    };
    description = "";
    after = [ "tailscaled.service" ];
    wantedBy = [ "multi-user.target" ]; # needed to enable during boot
    serviceConfig = {
      ExecStart = "${pkgs.local.boat-coordinator.frontroom-down-lights}/bin/frontroom-down-lights";
      Restart = "always";
      RestartSec = 60;
      StartLimitIntervalSec = "300"; # it has a 10 min retry internally, if something is failing before that there is a bigger problem so setting this to 5 min
      # StartLimitAction =  # todo work out a nice thing to trigger if start limit it hit, maybe a push notification
    };
  };
  systemd.services."dinette-down-lights" = {
    enable = true;
    environment = {
      LOG_TOKEN_FILE = config.sops.secrets.influx-log-token.path;
    };
    description = "";
    after = [ "tailscaled.service" ];
    wantedBy = [ "multi-user.target" ]; # needed to enable during boot
    serviceConfig = {
      ExecStart = "${pkgs.local.boat-coordinator.dinette-down-lights}/bin/dinette-down-lights";
      Restart = "always";
      RestartSec = 60;
      StartLimitIntervalSec = "300"; # it has a 10 min retry internally, if something is failing before that there is a bigger problem so setting this to 5 min
      # StartLimitAction =  # todo work out a nice thing to trigger if start limit it hit, maybe a push notification
    };
  };
  systemd.services."bathroom-down-lights" = {
    enable = true;
    description = "";
    environment = {
      LOG_TOKEN_FILE = config.sops.secrets.influx-log-token.path;
    };
    after = [ "tailscaled.service" ];
    wantedBy = [ "multi-user.target" ]; # needed to enable during boot
    serviceConfig = {
      ExecStart = "${pkgs.local.boat-coordinator.bathroom-down-lights}/bin/bathroom-down-lights";
      Restart = "always";
      RestartSec = 60;
      StartLimitIntervalSec = "300"; # it has a 10 min retry internally, if something is failing before that there is a bigger problem so setting this to 5 min
      # StartLimitAction =  # todo work out a nice thing to trigger if start limit it hit, maybe a push notification
    };
  };
  systemd.services."kitchen-down-lights" = {
    enable = true;
    environment = {
      LOG_TOKEN_FILE = config.sops.secrets.influx-log-token.path;
    };
    description = "";
    after = [ "tailscaled.service" ];
    wantedBy = [ "multi-user.target" ]; # needed to enable during boot
    serviceConfig = {
      ExecStart = "${pkgs.local.boat-coordinator.kitchen-down-lights}/bin/kitchen-lights";
      Restart = "always";
      RestartSec = 60;
      StartLimitIntervalSec = "300"; # it has a 10 min retry internally, if something is failing before that there is a bigger problem so setting this to 5 min
      # StartLimitAction =  # todo work out a nice thing to trigger if start limit it hit, maybe a push notification
    };
  };
  systemd.services."bedroom-down-lights" = {
    enable = true;
    environment = {
      LOG_TOKEN_FILE = config.sops.secrets.influx-log-token.path;
    };
    description = "";
    after = [ "tailscaled.service" ];
    wantedBy = [ "multi-user.target" ]; # needed to enable during boot
    serviceConfig = {
      ExecStart = "${pkgs.local.boat-coordinator.bedroom-down-lights}/bin/bedroom-down-lights";
      Restart = "always";
      RestartSec = 60;
      StartLimitIntervalSec = "300"; # it has a 10 min retry internally, if something is failing before that there is a bigger problem so setting this to 5 min
      # StartLimitAction =  # todo work out a nice thing to trigger if start limit it hit, maybe a push notification
    };
  };
  systemd.services."engineroom-down-lights" = {
    enable = true;
    environment = {
      LOG_TOKEN_FILE = config.sops.secrets.influx-log-token.path;
    };
    description = "";
    after = [ "tailscaled.service" ];
    wantedBy = [ "multi-user.target" ]; # needed to enable during boot
    serviceConfig = {
      ExecStart = "${pkgs.local.boat-coordinator.engineroom-down-lights}/bin/engineroom-down-lights";
      Restart = "always";
      RestartSec = 60;
      StartLimitIntervalSec = "300"; # it has a 10 min retry internally, if something is failing before that there is a bigger problem so setting this to 5 min
      # StartLimitAction =  # todo work out a nice thing to trigger if start limit it hit, maybe a push notification
    };
  };

  systemd.services."wardrobe" = {
      enable = true;
      environment = {
        LOG_TOKEN_FILE = config.sops.secrets.influx-log-token.path;
      };
      description = "";
      after = [ "tailscaled.service" ];
      wantedBy = [ "multi-user.target" ]; # needed to enable during boot
      serviceConfig = {
        ExecStart = "${pkgs.local.boat-coordinator.wardrobe}/bin/wardrobe";
        Restart = "always";
        RestartSec = 60;
        StartLimitIntervalSec = "300"; # it has a 10 min retry internally, if something is failing before that there is a bigger problem so setting this to 5 min
        # StartLimitAction =  # todo work out a nice thing to trigger if start limit it hit, maybe a push notification
      };
    };

  systemd.services."boat-occupied" = {
    enable = true;
    environment = {
      LOG_TOKEN_FILE = config.sops.secrets.influx-log-token.path;
    };
    description = "";
    after = [ "tailscaled.service" ];
    wantedBy = [ "multi-user.target" ]; # needed to enable during boot
    serviceConfig = {
      ExecStart = "${pkgs.local.boat-coordinator.boat-occupied}/bin/boat_occupied";
      Restart = "always";
      RestartSec = 60;
      StartLimitIntervalSec = "300"; # it has a 10 min retry internally, if something is failing before that there is a bigger problem so setting this to 5 min
      # StartLimitAction =  # todo work out a nice thing to trigger if start limit it hit, maybe a push notification
    };
  };

  systemd.services."boat-occupied-lights" = {
    enable = true;
    environment = {
      LOG_TOKEN_FILE = config.sops.secrets.influx-log-token.path;
    };
    description = "";
    after = [ "tailscaled.service" ];
    wantedBy = [ "multi-user.target" ]; # needed to enable during boot
    serviceConfig = {
      ExecStart = "${pkgs.local.boat-coordinator.boat-occupied-lights}/bin/boat_occupied_lights";
      Restart = "always";
      RestartSec = 60;
      StartLimitIntervalSec = "300"; # it has a 10 min retry internally, if something is failing before that there is a bigger problem so setting this to 5 min
      # StartLimitAction =  # todo work out a nice thing to trigger if start limit it hit, maybe a push notification
    };
  };

  systemd.services."bedroom-tap-dial" = {
    enable = true;
    environment = {
      LOG_TOKEN_FILE = config.sops.secrets.influx-log-token.path;
    };
    description = "";
    after = [ "tailscaled.service" ];
    wantedBy = [ "multi-user.target" ]; # needed to enable during boot
    serviceConfig = {
      ExecStart = "${pkgs.local.boat-coordinator.bedroom-tap-dial}/bin/bedroom-tap-dial";
      Restart = "always";
      RestartSec = 60;
      StartLimitIntervalSec = "300"; # it has a 10 min retry internally, if something is failing before that there is a bigger problem so setting this to 5 min
      # StartLimitAction =  # todo work out a nice thing to trigger if start limit it hit, maybe a push notification
    };
  };

 }
