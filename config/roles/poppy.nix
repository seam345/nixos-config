{ config, pkgs, lib, ... }:
let
  home-manager = builtins.fetchGit {
    url = "https://github.com/nix-community/home-manager";
    rev = "6d3912c6dbe153e0826cf271a5403455d1b47f49";
    ref = "release-22.11";
  };
in
{
  users.users.poppy = {
    isNormalUser = true;
    linger = true;
    home = "/home/poppy";
    description = "Poppy";
    #    shell = pkgs.nushell; # drop back to bash I think
    extraGroups = [ "wheel" "networkmanager" "scanner" "lp" "dialout" ];
    openssh.authorizedKeys.keys = [ ];
    initialPassword = "password";
  };

  home-manager.users.poppy = {
    home = { stateVersion = "22.11"; };
    ## todo discus if this is what poppy wants
    programs.git = {
      enable = true;
      ignores = [ "*.idea" "venv/" ];
      extraConfig = {
        core = { editor = "vim"; };
        help = { autocorrect = "25"; };
      };
    };
    programs.bash = {
      enable = true;
      bashrcExtra = '''';
      historyControl = [ "erasedups" ];
      historyIgnore = [ "exit" "'history -w'" ];
      initExtra = ''
        # used to sync my deployed nixpkgs with nix shell, this is not perfect for instance if I roll back or if I ran krops and it synced but failed uild it will be out of sync
        alias nix-shell="nix-shell -I nixpkgs=/var/src/nixpkgs/"
      '';
    };
  };

  # Syncthing as user systemd service
  services.syncthing.enable = true;
  services.syncthing.systemService = false;
  networking.firewall.allowedTCPPorts = [ 8385 22000 ];
  networking.firewall.allowedUDPPorts = [ 22000 21027 ];
  home-manager.users.poppy = {
    services.syncthing.enable = true;
    services.syncthing.extraOptions = [
      "--gui-address=0.0.0.0:8385"
    ];
  };

  security.chromiumSuidSandbox.enable = true;
  environment.systemPackages = with pkgs; [
    unityhub
  ];
}


