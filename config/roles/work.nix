{ lib, pkgs, ... }:
let
  home-manager = builtins.fetchGit {
    url = "https://github.com/nix-community/home-manager";
    rev = "2860d7e3bb350f18f7477858f3513f9798896831";
    ref = "release-21.11";
  };
  yubikeyPubSSHKey = "${import ../vars/ssh-public-keys/yubikey.nix}";
  x390PubSSHKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDF2KloKNkyp6dhRX07+G5AVCvc1Ac6h8Y+d6PsFxly/gzsvaJ9X8dB20fIfxWYWKUWVS6Ju0ipe3VPfS8FJWQsaoh5pPEQFG+vlFffiHrK+aRoMGGulTXCAlM4O1aaVBdrQ6IZ7PnTUJ3GbwKpDaTlweLneDcMNdtyi9WXdBylYj14Hru4/TyAHDgbTGofBdR7p8HzfIaU9TlfTqURUKRqKBddLWHRz2uOGFfSl5FtFd36pWvruNP7oK0itbNYUYZowBNrAnXGHe3Gv/olxZS2Ism9E20VUgNfSdHQDDR1g8Q17zLf0R9njMVO2i2DhBCHtZqqIIdAlMjkV/MZ9If1OPY0vXm3R0iank8uw7kl64Gmm71xBzRUAtiY47To3SNBxBgjiwaeW+FeG3/jywHnE4lZNBIEe4pNFLCGXUGTyOW8xM/8SWXdaRc842wxYgFrPlpO6CM/ZF1amY1Q1Yt0FwFKFDN5uw76nYgfl6RzVJzV5Vkh4DGa4quctLTHwp0= sean@work-x390";
in
{
  # Increase security of system
  security.pam.yubico = {
    enable = true;
    #debug = true;
    control = "required"; # set this to require a password aswell TODO setup for work laptop
    mode = "challenge-response";
  };

  # reduce authorised keys for krops
  users.users.krops = lib.mkForce {
    # remember krops needs to have access to /var/src
    isNormalUser = true;
    home = "/home/krops";
    description = "krops";
    openssh.authorizedKeys.keys = [
      "${x390PubSSHKey}"
      "${yubikeyPubSSHKey}"
    ];
  };

  # remove all authorised keys for root
  users.users.root.openssh.authorizedKeys.keys = lib.mkForce [ ];

  home-manager.users.sean = {
    programs.git = {
      userEmail = "sean.borg@codethink.co.uk";
      userName = "Sean Borg";
    };
  };

  environment.systemPackages = with pkgs; [
    quasselClient
    thunderbird
    pass
    oathToolkit
  ];
}
