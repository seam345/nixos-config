{ config, pkgs, ... }:
{
  imports = [
    "${builtins.fetchTarball {
      url = "https://github.com/Mic92/sops-nix/archive/49a87c6c827ccd21c225531e30745a9a6464775c.tar.gz";
      sha256 = "19l0rsxnhbam9yb08yrr2g41fz9qwkbp394mj8dl4cyqza1dfzpw";
    }}/modules/sops"
  ];
}

# on system
# nix-shell -p ssh-to-pgp --run "ssh-to-pgp -i /etc/ssh/ssh_host_rsa_key -o server01.asc"
