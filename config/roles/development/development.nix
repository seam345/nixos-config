{ config, pkgs, lib, ... }:
let
  myXmodmap = pkgs.writeText "xkb-layout" ''
    keycode 107 = Super_R NoSymbol Super_R
  '';
  importJson = (import <nixpkgs> { }).lib.importJSON;
  home-manager = builtins.fetchGit {
    # update with: nix-shell -p nix-prefetch-git --command 'nix-prefetch-git   --url https://github.com/nix-community/home-manager   --rev refs/heads/master   > config/channels/home-manager.json'
    rev = (importJson ../../channels/home-manager.json).rev;
    url = (importJson ../../channels/home-manager.json).url;
    allRefs = true;
  };
in
{
  # Syncthing
  services.syncthing.enable = true;
  services.syncthing.systemService = false;
  networking.firewall.allowedTCPPorts = [
    #8384
    22000
  ];
  networking.firewall.allowedUDPPorts = [ 22000 21027 ];
  home-manager.users.sean = {
    services.syncthing.enable = true;
    services.syncthing.extraOptions = [
      "--gui-address=0.0.0.0:8384"
    ];
  };


  programs.adb.enable = true;
  users.users.sean = {
    isNormalUser = true;
    linger = true;
    home = "/home/sean";
    description = "myself";
    shell = pkgs.fish;
    extraGroups = [ "adbusers" "wheel" "networkmanager" "scanner" "lp" "dialout" ];
    openssh.authorizedKeys.keys = [
      "${import ../../vars/ssh-public-keys/yubikey.nix}"
      #"${import ../../vars/ssh-public-keys/lenovo-laptop.nix}"
      "${import ../../vars/ssh-public-keys/pixel4a.nix}"
      "${import ../../vars/ssh-public-keys/pc.nix}"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOrYwbuSUQzBb7X2AFTkDfrmAicT1jLvyJUpE/wNB94e ShellFish@iPad-18082024"
    ];
  };
  services.udev.packages = with pkgs; [
    yubikey-personalization
  ];

  systemd.user = {
    services."remap-left-hand-mouse" = {
      path = [ pkgs.xorg.xinput];
      wantedBy = ["dev-input-by\\x2did-usb\\x2dELECOM_ELECOM_TrackBall_Mouse\\x2devent\\x2dmouse.device"];
      environment = {
        RUST_LOG = "trace";
      };
      description = "Remap left hand mouse";
      serviceConfig = {
        ExecStart = "${pkgs.local.leftHandMouseRemap}/bin/left-hand-mouse-remap";
      };
    };
  };
  programs = {
    ssh.startAgent = false;

    ssh.extraConfig = "${import ../../vars/ssh_config.nix}";
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
  };
  services.pcscd.enable = true; # For yubikey manager https://github.com/NixOS/nixpkgs/issues/51362

  virtualisation.docker.enable = true;
  imports = [
    (import "${home-manager}/nixos")
  ];

  home-manager.users.sean = {
    home = { stateVersion = "22.11"; };
    # initial start to test permission updator on plane, inout file needs to be a thing nix knows changes otherwise it wont be reran
    #    home.activation = {
    #      firefox-permission-updater-activation = ''
    #         /home/sean/GitDirs/Gitlab/Seam345/firefox-permission-updater/target/debug/firefox-permission-updater
    #      '';
    #    };
    xdg.configFile = {
      "awesome" = {
        source = ./awesome/plain;
        target = "./awesome";
        recursive = true; # i reckon this is needed for the bellow to work, not tested though
      };
      "awesome/rc.lua" = {
        text = (import ./awesome/nix/rc.nix) { icon-theme = pkgs.arc-icon-theme; pkgs = pkgs; };
        target = "./awesome/rc.lua";
      };
      "nushell" = {
        source = ./nushell/plain;
        target = "./nushell";
        recursive = true;
      };
      "zellij/layouts" = {
        source = ./zellij/plain;
        target = "./zellij/layouts";
        recursive = true;
      };
      "fish" = {
        source = ./fish/plain;
        target = "./fish/";
        recursive = true;
      };
      "atuin" = {
        source = ./atuin/plain;
        target = "./atuin/";
        recursive = true;
      };
    };
    home.file = {
      "firefox" = {
        source = ./firefox/plain;
        target = "./.mozilla/firefox";
        recursive = true;
      };
    };
    programs.zellij.enableFishIntegration = true;
    programs.git = {
      enable = true;
      lfs.enable = true;
      ignores = [ "*.idea" "venv/" ];
      extraConfig = {
        core = { editor = "vim"; };
        help = { autocorrect = "25"; };
        safe = { directory = "*"; }; # I control the whole computer, it's worth the trade off
      };
    };
    programs.bash = {
      enable = true;
      bashrcExtra = '''';
      historyControl = [ "erasedups" ];
      historyIgnore = [ "exit" "'history -w'" ];
      initExtra = ''
        export PS1='[\u@\h \W \T]\$ '
        # used to sync my deployed nixpkgs with nix shell, this is not perfect for instance if  roll back or if I ran krops and it synced but failed uild it will be out of sync
        alias nix-shell="nix-shell -I nixpkgs=/var/src/nixpkgs/"
      '';
    };
  };
  #----=[ Fonts ]=----#
  fonts = {
    enableDefaultFonts = true;
    fonts = with pkgs; [
      pkgs.ubuntu_font_family
      pkgs.victor-mono # for rust manchester
      pkgs.zilla-slab # for rust manchester
      pkgs.texlivePackages.alfaslabone # for rust manchester
      pkgs.fira-sans # for rust manchester
    ];
    fontconfig = {
      defaultFonts = {
        serif = [ "Ubuntu" ];
        sansSerif = [ "Ubuntu" ];
        monospace = [ "Ubuntu Mono" ];
      };
    };
  };

  virtualisation.libvirtd.enable = true;

  services.printing = { enable = true; };
  services.printing.drivers = [ pkgs.samsung-unified-linux-driver pkgs.splix ];
  services.avahi = {
    enable = true;
    nssmdns = true;
    openFirewall = true;
  };
  hardware.sane.enable = true;
  #  users.users.sean.extraGroups = [ "scanner" "lp" ];

  environment.etc = {
    "testAutoStart" = {
      text = "Mod=Mod4";
      mode = "0777";
    };
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  # require startx
  #services.xserver.displayManager.startx.enable = true;
  #  services.xserver.desktopManager.mate.enable = true;
  programs.xss-lock.enable = true;
  services.xserver.windowManager.awesome.enable = true;
  #  services.xserver.windowManager.i3.enable = true;
  #  services.xserver.windowManager.i3.configFile = pkgs.writeText "i3config" "${import ./i3-config.nix}";

  # for steam
  hardware.opengl.driSupport32Bit = true;

  # Configure keymap in X11
  services.xserver.layout = "gb";
  # services.xserver.xkbOptions = "eurosign:e";

  services.xserver.displayManager.sessionCommands = "${pkgs.xorg.xmodmap}/bin/xmodmap ${myXmodmap}";

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  hardware.bluetooth.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;


  # Todo make something that sets this automatically
  # (c) For `nix-env`, `nix-build`, `nix-shell` or any other Nix command you can add
  #       { allowUnfree = true; }
  #     to ~/.config/nixpkgs/config.nix.
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  nixpkgs.overlays = [
    (self: super: {
      local = import ./my-pkgs { pkgs = super; };
      #    jetbrains =
    })
  ];
  #  nixpkgs.overlays = [ (self: super: {   }) ];

  nixpkgs.config = {
    #  allowUnfree = true;
    packageOverrides = pkgs: {
      jetbrains-pinned = import
        (builtins.fetchTarball {
          # Descriptive name to make the store path easier to identify
          name = "nixos-pinned-jetbrains-2022.3";
          # Commit hash for nixos-unstable as of 2018-09-12
          url = "https://github.com/nixos/nixpkgs/archive/8c46c7ae2397286f1b3a0fd8fe0af4aef19800e9.tar.gz";
          # Hash obtained using `nix-prefetch-url --unpack <url>`
          sha256 = "0ippjwn3ri9dil5g8rm4335iann2m771ns0kvkf91rsw3mcf9lmv";
        })
        {
          config = config.nixpkgs.config;
        };
    };
  };

  programs.direnv.enable = true;
  
  environment.systemPackages = with pkgs; [
    firefox
    qt5.qttools
    xorg.xmodmap # needed or above command, I wonder if I can not add it here
    jetbrains-pinned.jetbrains.jdk
    jetbrains-pinned.jetbrains.idea-ultimate
    jetbrains-pinned.jetbrains.clion # for rust dev
    direnv
    nix-direnv
    xclip
    obsidian
    # Bellow 2 needed for obsidian ocr plugin
    tesseract
    imagemagick
    # Above 2 needed for obsidian ocr plugin
    wpa_supplicant_gui
    mate.mate-terminal
    pkgs.local.scripts
    hugin
    alacritty
    acpi
    arc-icon-theme
    broot
    ## the bellow should go into my bash scripts packages
    xdotool
    jq
    # for password stuff
    pass
    oath-toolkit
    passff-host
    atuin
    typos
  ];
  programs.firefox.nativeMessagingHosts.passff = true;
  nixpkgs.config.permittedInsecurePackages = [ # for obsidian and maybe some others
    "electron-25.9.0"
  ];

  environment.pathsToLink = [
    "/share/nix-direnv"
  ];

  qt.enable = true;
  qt.style = "adwaita-dark";
  qt.platformTheme = "gnome";

}


