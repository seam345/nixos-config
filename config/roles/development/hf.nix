{ config, pkgs, lib, ... }:
let
  myXmodmap = pkgs.writeText "xkb-layout" ''
    keycode 107 = Super_R NoSymbol Super_R
  '';
  importJson = (import <nixpkgs> { }).lib.importJSON;
  home-manager = builtins.fetchGit {
    # update with: nix-shell -p nix-prefetch-git --command 'nix-prefetch-git   --url https://github.com/nix-community/home-manager   --rev refs/heads/master   > config/channels/home-manager.json'
    rev = (importJson ../../channels/home-manager.json).rev;
    url = (importJson ../../channels/home-manager.json).url;
    allRefs = true;
  };
in
{
  users.users.handsfree = {
    isNormalUser = true;
    home = "/home/handsfree";
    description = "hf";
    shell = pkgs.fish;
    extraGroups = [ "adbusers" "wheel" "networkmanager" "scanner" "lp" "dialout" ];
    openssh.authorizedKeys.keys = [ "${import ../../vars/ssh-public-keys/yubikey.nix}" ];
  };

   # Syncthing
  services.syncthing.enable = true;
  services.syncthing.systemService = false;
  networking.firewall.allowedTCPPorts = [
    22000
  ];
  networking.firewall.allowedUDPPorts = [ 22000 21027 ];
  home-manager.users.handsfree = {
    services.syncthing.enable = true;
    services.syncthing.extraOptions = [
      "--gui-address=0.0.0.0:8385"
    ];
  };

  home-manager.users.handsfree = {
    home = { stateVersion = "22.11"; };
    # initial start to test permission updator on plane, inout file needs to be a thing nix knows changes otherwise it wont be reran
    #    home.activation = {
    #      firefox-permission-updater-activation = ''
    #         /home/sean/GitDirs/Gitlab/Seam345/firefox-permission-updater/target/debug/firefox-permission-updater
    #      '';
    #    };
    xdg.configFile = {
      "awesome" = {
        source = ./awesome/plain;
        target = "./awesome";
        recursive = true; # i reckon this is needed for the bellow to work, not tested though
      };
      "awesome/rc.lua" = {
        text = (import ./awesome/nix/rc.nix) { icon-theme = pkgs.arc-icon-theme; pkgs = pkgs; };
        target = "./awesome/rc.lua";
      };
      "nushell" = {
        source = ./nushell/plain;
        target = "./nushell";
        recursive = true;
      };
    };
    programs.git = {
      enable = true;
      lfs.enable = true;
      ignores = [ "*.idea" "venv/" ];
      extraConfig = {
        core = { editor = "vim"; };
        help = { autocorrect = "25"; };
        safe = { directory = "*"; }; # I control the whole computer, it's worth the trade off
      };
      userEmail = "sean@seanborg.tech";
      userName = "Sean Borg";
    };
    programs.bash = {
      enable = true;
      bashrcExtra = '''';
      historyControl = [ "erasedups" ];
      historyIgnore = [ "exit" "'history -w'" ];
      initExtra = ''
        export PS1='[\u@\h \W \T]\$ '
        # used to sync my deployed nixpkgs with nix shell, this is not perfect for instance if  roll back or if I ran krops and it synced but failed uild it will be out of sync
        alias nix-shell="nix-shell -I nixpkgs=/var/src/nixpkgs/"
      '';
    };
  };




}


