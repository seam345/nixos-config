{ config, pkgs, lib, ... }:
{
  networking.firewall.allowedTCPPorts = [ 2342 ];
  services.grafana = {
    enable = true;
#    port = 2342;
    # addr = assign this elsewhere;
  };
  services.grafana.settings.server.http_port = 2342;
  # to make this generic I left out addr for grafana
  # add
  # services.grafana.addr = "<ipaddress>"
  # to somewhere device specific not sure where yet

  systemd.services.grafana.after = lib.mkForce [ "tailscaled.service" ];
  systemd.services.grafana.serviceConfig = {
    RestartSec = 60;
    Restart = lib.mkForce  "always" ;
  };
  systemd.services.grafana.startLimitIntervalSec = 0;
}
