{ config, pkgs, ... }:
{
  imports = [
    ./sops.nix
  ];
  sops.secrets.networking-env-file = {
    sopsFile = ./wifi-secrets.yaml;
  };

  networking.wireless.environmentFile = config.sops.secrets.networking-env-file.path;
  #networking.networkmanager.enable = true;
  networking.wireless.enable = true;
  networking.wireless.userControlled.enable = true;

  networking.wireless.networks = {
    "Codethink Guest" = {
      psk = "@GUEST_PASS@";
    };
    "tranquility" = {
      psk = "@TRANQUILITY_PASS@";
    };
    "auto-tranquil" = {
      psk = "@AUTO_TRANQUILITY_PASS@";
    };
    "BTHub5-SHG7" = {
      psk = "@BTHUB5_SHG7_PASS@";
    };
    "iphone" = {
      priority = 11;
      psk = "12345678";
    };
    "BTHub5-23R3" = {
      psk = "@BTHUB5_23R3@";
    };
    "VM3728053" = {
      psk = "@VM3728053@";
    };
    "PLUSNET-M63K" = {
      psk = "@PLUSNET_M63K@";
    };
    "BT-S7A886" = {
      psk = "@BT_S7A886@";
    };
    "DragonsLair" = {
      psk = "@DRAGONSLAIR@";
    };
    "TALKTALK-B5B742" = {
      psk = "@TALKTALK_B5B742@";
    };
    "BT-PHAWTQ" = {
      psk = "@BT_PHAWTQ@";
    };
    "HFG-Guest-UK" = {
      psk = "@HFG_GUEST@";
    };
    "CottagePie" ={
      psk = "@COTTAGE_PIE@";

    };
    "M-Guest" ={
      pskRaw = "@M_GUEST@"; # Got this value using wpa_passphrase ESSID PSK
    };
    #"Beehive5g" = {
      #psk = "@BEHIVE_TEST@";
    #};
    #"Beehive Lofts" = {
      #psk = "@BEHIVE_TEST@";
    #};

    #"Beehive Upper" = {
      #psk = "@BEHIVE_TEST@";
    #};
    "Beehive Lower" = {
      psk = "@BEHIVE_TEST@";
#      pskRaw = "@BEHIVE_LOFTS@";
    };
    "Mitchell's WiFi 5G"={
      psk="@MITCH@";
    };
    "VPL"={
    };
    "YYC-Free-WiFi"={
    };
    "_Chiltern-wifi"={
    };
    "BT-KTA82J"={
      psk="3Ormiston";
    };

    "Grain-1245347"={
      psk="@GRAIN_1245347@";
    };


    "Personal darkness"={
      psk="@PERSONAL_DARKNESS@";
    };

    "LochinshWiFi_Guest"={
      psk="Sk11ng16";
    };


    "Guest"={
    };

    "citizenM"={
    };

    "Graythwaite"={
    };
    "emf2024-open"={
    };

  };
}
