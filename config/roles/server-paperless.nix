{ config, pkgs, ... }:
let
  dataDir = "/var/lib/paperless";
  paperlessBackup = "paperlessBackup";
in
{
  imports = [
    ./sops.nix
  ];
  sops.secrets.paperless-admin-password = {
    sopsFile = ./server-paperless-secrets.yaml;
  };
  services.paperless.enable = true;
  services.paperless.address = "${import ../vars/ips/pc.nix}"; # used to work over tailscale
  services.paperless.passwordFile = config.sops.secrets.paperless-admin-password.path;

  # Backup service

  # dataDir set with variable to keep it inline with backup service
  services.paperless.dataDir = "${dataDir}";
  # Setting user to me so when I make a backup I can move it around with syncthing.
  # option 2 was having another syncthing under the paperless user but thats another imperative thing I cba managing
  services.paperless.user = "sean";

  # Backup service + timer
  systemd = {
    services."${paperlessBackup}" = {
      description = "Backsup the paperless data";
      serviceConfig = {
        # User/Group set to not fuckup permissions in my home directory, and to allow syncthing to send it offsite
        User = "sean";
        Group = "users";
        ExecStart = "${dataDir}/paperless-manage document_exporter /home/sean/storage/paperless/ --delete --use-folder-prefix";
      };
    };

    timers."${paperlessBackup}" = {
      wantedBy = [ "multi-user.target" ];
      timerConfig = {
        OnCalendar = "*-*-* 03:05:00"; # every day at 03:05 min
        Unit = "${paperlessBackup}.service";
      };
    };
  };
}
