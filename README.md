# Welcome to my Nixos config!

This is my personal Nixos configuration for all my linux computers


# CI/CD
On all unprotected branches not staging/deployed I run the nixpkgs-fmt 

on protected branches staging/deployed I confirm my systems are on the latest branch

# Git convention
following a similar style to conventional commits but with different types as a lot of them make no sense

for git commit hints run `git config commit.template .gitmessage.txt`
